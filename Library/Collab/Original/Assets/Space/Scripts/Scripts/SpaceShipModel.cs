﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpaceShipModel : MonoBehaviour
{
    //public Animator animator;
    private int count = 0;
    private GameObject destination;
    private float intensity = 0f;
    private float target = 1f;
    public static float distanceFromDestination;
    private FirstScene fs;
    private GameObject weapon;
    private GameObject spaceShip;
    public GameObject spotLight;
    public GameObject finalDestination;
    Scene m_Scene;
    string sceneName;
    private static float angleFromDestination;

    // Start is called before the first frame update
    void Start()
    {
      //  animator = GetComponent<Animator>();
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;
        GameObject firstScene = GameObject.Find("FirstScene");
        weapon = GameObject.Find("weapon");
        spaceShip = GameObject.Find("spaceShip");
        finalDestination = GameObject.Find("Colliding Meteors");
        //spotLight = GameObject.Find("Spot Light");
        if (firstScene != null)
            fs = firstScene.GetComponent<FirstScene>();
    }

    // Update is called once per frame
    void Update()
    {

        if (destination == null)
        {
            if (sceneName == "floating")
            {
                destination = GameObject.Find("Turtle");
                if (destination == null)
                {
                    Debug.Log("Turtle dead");
                    destination = GameObject.Find("SuperFuel");
                }
            }
            else
            {
                destination = fs.targetPlanet;
            }
            return;
        }
        else
        {
            this.transform.forward = (destination.transform.position - this.transform.position).normalized;
            distanceFromDestination = Vector3.Distance(destination.transform.position, this.transform.position);
            intensity = Mathf.MoveTowards(intensity, target, Time.deltaTime);
            if (intensity == 1)
                target = 0;
            if (intensity == 0)
                target = 1;
            this.GetComponent<Renderer>().sharedMaterial.SetFloat("_GlowIntensity", intensity);

            if(destination == GameObject.Find("SuperFuel") && distanceFromDestination<650)
            {
                Debug.Log("Spot light activated");
                spotLight.SetActive(true);
            }
            weapon.transform.rotation = Quaternion.Euler(-1 * this.transform.localRotation.eulerAngles.y, -90, 90);
        }

        //StartCoroutine(RotateImage());
        //StartCoroutine(RotateImage(-180));
        /*if (count == 0)
        {
            RotateModel(180,1f);
            if(this.transform.eulerAngles.y > 178)
            {
                count++;
            }
        }
        else if(count==1)
        {
            RotateModel(0,1f);
            if (this.transform.eulerAngles.y < 2)
            {
                count++;
            }
        }
        else if (count == 2)
        {
            RotateModel(90,1f);
            if (this.transform.eulerAngles.y > 89)
            {
                count++;
            }
        }
        else
        {
            this.transform.Rotate(0,Time.deltaTime * 50,0);
        }*/
    }

    IEnumerator RotateImage()
    {
        float Angle = 180;
        float moveSpeed = 0.0001f;
        float y = Angle;
        while (this.transform.rotation.y < Angle)
        {
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.Euler(0, Angle, 0), moveSpeed * Time.time);
            yield return null;
        }
        this.transform.rotation = Quaternion.Euler(0, Angle, 0);
        yield return null;
    }

    void RotateModel(int Angle,float speed)
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, Angle, 0), Time.deltaTime * speed);
    }
}
