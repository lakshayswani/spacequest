﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Positron;


public class CylinderHealthBar : MonoBehaviour
{
    Animator anim;
    private int prev_coll = 0;
    private GameObject Cylinder;
    private GameObject forcefield;
    private GameObject crashlight;
    private GameObject collisionSound;
    
    private Vector3 startsize;
    private Vector3 endsize;
    private Vector3 startpos;
    private Vector3 endpos;
    private GameObject canvas;
    private bool  ShowGameOver = false;
    private GameObject spaceShip;
    public GameObject explosion;

    private ChairInputWrapper chairWrapper;
    private float pitch_vel = 54f;
    private float pitch_acc = 54f;
    private float yaw_vel = 36f;
    private float yaw_acc = 36f;

    [HideInInspector]
    public static int lives = 8;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("TestCallStack: {0}" + Environment.StackTrace);
        //Console.WriteLine("Test stack: {0}", Environment.StackTrace);
        Debug.Log(typeof(CylinderHealthBar).BaseType.Name);
        Debug.Log("Start entered in cylinder healthbar");
        Cylinder = GameObject.Find("Cylinder");
        anim = Cylinder.GetComponent<Animator>();
        Debug.Log("Start before lives");
        //lives = 5;
        Debug.Log("Right after lives");
        startsize = Cylinder.transform.localScale;
        endsize = new Vector3(0.035f, 0.0f, 0.035f);
        startpos = Cylinder.transform.localPosition;
        endpos = new Vector3(-0.565f, 0f, 2.224f);
        forcefield = GameObject.Find("ForceField");
        crashlight = GameObject.Find("CrashLight");
        collisionSound = GameObject.Find("CollisionSound");
        spaceShip = GameObject.Find("spaceShip");
        GameObject vm = GameObject.Find("Voyager Manager");
        if (vm != null)
        {
            chairWrapper = vm.GetComponent<ChairInputWrapper>();
            chairWrapper.Yaw_factor = 3f;
            chairWrapper.Pitch_chair_angle = 8f;
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, chairWrapper.Pitch_vel, chairWrapper.Pitch_acc));
        }
    }


    void FadeOut()
    {

       
        Renderer rend = forcefield.GetComponent<Renderer>();
        Color c = rend.materials[0].color;
        float temp = c.a;

        for (float f=c.a; f>=-0.05f; f -= 0.05f)
        {
            c.a = Mathf.Clamp(f, 0, 1);
            rend.materials[0].color = c;
        }
        rend.enabled = false;
        c.a = Mathf.Clamp(temp, 0, 1);
        rend.materials[0].color = c;
    }


    IEnumerator Flicker()
    {
        //forcefield = GameObject.Find("ForceField");
        Debug.Log("Collision: Flicking entered");
        //forcefield.GetComponent<MeshRenderer>().enabled = true;
        Debug.Log("Collision: Flicking start Loop");
        for (int i=0; i<2; i++)
        {
            Debug.Log("Collision: Flicking");
            forcefield.GetComponent<MeshRenderer>().enabled = !forcefield.GetComponent<MeshRenderer>().enabled;
            crashlight.GetComponent<Light>().enabled = !crashlight.GetComponent<Light>().enabled;
            yield return new WaitForSeconds(1.0f);
        }
        forcefield.GetComponent<MeshRenderer>().enabled = false;
        Debug.Log("Collision: Flicking End");
        yield break;
    }

    IEnumerator gameover()
    {


        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "GameOver")
            {
                canvas = child.gameObject;
                break;
            }
        }
        Debug.Log("reached coroutine");
        canvas.SetActive(true);
        yield return new WaitForSeconds(2.0f);
    }
    void OnCollisionEnter(Collision other)
    {
        Debug.Log(this.name);
        if ((other.gameObject.tag == "asteroid" || other.gameObject.tag == "Fireball") && !ShipInput.forceFieldEngaged)
        {
            Debug.Log("lives: " + lives);
            StartCoroutine(StartJerk());
            
            Debug.Log(this.name);
            if (other.gameObject.GetInstanceID() != prev_coll)
            {
                prev_coll = other.gameObject.GetInstanceID();
                Debug.Log("Collision detected from healthbar called from Cylinder health bar");

                collisionSound.GetComponent<AudioSource>().Play();
                //forcefield.GetComponent<MeshRenderer>().enabled = true;
                //FadeOut();
                //StartCoroutine(Flicker());
                /*
                if (lives==3)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    anim.SetTrigger("hit1");
                    Debug.Log("hit1");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 0.3334f);
                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0567f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 2.3847f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(255, 69, 0));
                    //StartCoroutine(Flicker());
                }

                else if(lives==2)
                {
                    StartCoroutine(Flicker()); 
                    lives--;
                    anim.SetTrigger("hit2");
                    Debug.Log("hit2");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 0.6667f);
                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0284f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 2.3564f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(200, 0, 0));
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_Color", new Color(255, 0, 0));
                }
                else if(lives==1)
                {
                    StartCoroutine(Flicker());

                    lives--;
                    anim.SetTrigger("hit3");
                    Debug.Log("hit3");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 1f);

                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 0.0f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(220, 20, 60));

                }
                else if(lives==0)
                {
                    Debug.Log("0 lives");
                    Cylinder.transform.localScale = new Vector3(0, 0, 0);
                    ShowGameOver= true;
                    StartCoroutine(gameover());
                    spaceShip.GetComponent<Ship>().canControl = false;

                }*/

                if (lives == 5)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    anim.SetTrigger("hit1");
                    Debug.Log("hit1");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 0.3334f);
                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0800f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 2.400f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(255, 69, 0));
                    //StartCoroutine(Flicker());
                }
                else if (lives == 4)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    anim.SetTrigger("hit2");
                    Debug.Log("hit2");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 0.6667f);
                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0525f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 2.355f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(200, 0, 0));
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_Color", new Color(255, 0, 0));
                }
                else if (lives == 3)
                {
                    StartCoroutine(Flicker());

                    lives--;
                    anim.SetTrigger("hit3");
                    Debug.Log("hit3");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 1f);

                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0383f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 2.3263f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(220, 20, 60));

                }
                else if (lives == 2)
                {
                    StartCoroutine(Flicker());

                    lives--;
                    anim.SetTrigger("hit4");
                    Debug.Log("hit4");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 1f);

                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0141f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 2.021f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(220, 20, 60));

                }
                else if (lives == 1)
                {
                    StartCoroutine(Flicker());

                    lives--;
                    anim.SetTrigger("hit5");
                    Debug.Log("hit5");
                    //Cylinder.transform.localScale = Vector3.Lerp(startsize, endsize, 1f);

                    Cylinder.transform.localScale = new Vector3(0.035f, 0.0f, 0.035f);
                    Cylinder.transform.localPosition = new Vector3(-0.565f, 0.0f, 2.224f);
                    //heart.transform.GetChild(0).transform.GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(220, 20, 60));

                }
                else if (lives == 0)
                {
                    Debug.Log("0 lives");
                    Cylinder.transform.localScale = new Vector3(0, 0, 0);
                    ShowGameOver = true;
                    StartCoroutine(gameover());
                    spaceShip.GetComponent<Ship>().canControl = false;

                }

                Debug.Log("collision");


                //if (Cylinder.transform.localScale.y == 0.0001f)
                //{
                //  Cylinder.transform.localScale = new Vector3(0, 0, 0);
                //SceneManager.LoadScene(0);
                //}
            }
        }
        else if((other.gameObject.tag == "asteroid" || other.gameObject.tag == "Fireball") && ShipInput.forceFieldEngaged)
        {
            Debug.Log("Collision detected from shield and asteroid");
            Debug.Log(other.gameObject.name);
            var exp = Instantiate(explosion, other.gameObject.transform.position, other.gameObject.transform.rotation);
            Destroy(exp, 2.0f);
            if(other.gameObject.tag == "asteroid")
            {
                Destroy(other.gameObject);
            }
        }

        /*else if()
        {

        }*/
    }
    private float endTime = 0f;

    // Update is called once per frame
    void Update()
    {
        if(ShowGameOver)
        {
            endTime += Time.deltaTime;
            if (endTime > 5.0f)
            {
                GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_1");
            }
        }
        
    }

    IEnumerator StartJerk()
    {
        if (chairWrapper != null)
        {
            chairWrapper.Yaw_chair_angle = -2.0f;
            VoyagerDevice.Yaw(new Vector3(chairWrapper.Yaw_chair_angle, yaw_vel, yaw_acc));
            chairWrapper.Pitch_chair_angle = 20f;
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, pitch_vel, pitch_acc));
            yield return new WaitForSeconds(0.2f);
            chairWrapper.Yaw_chair_angle = 0.0f;
            VoyagerDevice.Yaw(new Vector3(chairWrapper.Yaw_chair_angle, yaw_vel, yaw_acc));
            chairWrapper.Pitch_chair_angle = 21f;
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, pitch_vel, pitch_acc));
            yield return new WaitForSeconds(0.2f);
            chairWrapper.Yaw_chair_angle = -2.0f;
            VoyagerDevice.Yaw(new Vector3(chairWrapper.Yaw_chair_angle, yaw_vel, yaw_acc));
            chairWrapper.Pitch_chair_angle = 20f;
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, pitch_vel, pitch_acc));
            yield return new WaitForSeconds(0.2f);
            chairWrapper.Yaw_chair_angle = 0.0f;
            VoyagerDevice.Yaw(new Vector3(chairWrapper.Yaw_chair_angle, yaw_vel, yaw_acc));
            chairWrapper.Pitch_chair_angle = 21f;
            VoyagerDevice.Pitch(new Vector3(chairWrapper.Pitch_chair_angle, pitch_vel, pitch_acc));
        }
    }
}
