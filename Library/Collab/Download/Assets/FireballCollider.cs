﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballCollider : MonoBehaviour
{
    public GameObject explosion;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("fireball hit spaceship");
            var exp = Instantiate(explosion, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            //Destroy(collision.gameObject);
    
            Destroy(exp, 2.0f);
            Destroy(gameObject, 2.0f);
        }
    }
}
