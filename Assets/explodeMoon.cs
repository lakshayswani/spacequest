﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explodeMoon : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject BrokenMoon;
    void Start()
    {
        GameObject.Find("Moon").SetActive(false);
        Debug.Log("Broken moon is being called");
        BrokenMoon.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
