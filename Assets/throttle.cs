﻿using UnityEngine;
using System.Collections;

public class throttle : MonoBehaviour
{
	private Object[] throttleObjects;
	private Texture[] throttleTextures;
	private Material throttleMaterial;
	public int throttleVal;
	private float opacity = 1f;

	void Awake()
	{
		//Get a reference to the Material of the game object this script is attached to
		this.throttleMaterial = this.GetComponent<Renderer>().material;
		this.GetComponent<Renderer>().material.color = new Color(this.GetComponent<Renderer>().material.color.r, this.GetComponent<Renderer>().material.color.b, this.GetComponent<Renderer>().material.color.g, .65f * opacity);
	}

	void Start()
	{
		this.throttleObjects = Resources.LoadAll("Holographic/output/main/armor", typeof(Texture));
		this.throttleTextures = new Texture[14];
		int k = 13;
		for (int i = 0; i < 14; i++)
		{
			this.throttleTextures[i] = (Texture)this.throttleObjects[k];
			k = k - 1;
		}

		Transform throttleChildren = GameObject.Find("GUI Camera/GUI/health").GetComponentInChildren<Transform>(true);
		foreach (Transform child in throttleChildren)
		{
			if (child.gameObject.name == "throttle")
			{
				throttleMaterial = child.gameObject.GetComponent<Renderer>().material;
				break;
			}
		}
	}

	void OnGUI()
	{
	}

	void Update()
	{
		throttleVal = (int)ShipInput.throttle_GUI*5;
        if (throttleVal > 14)
        {
            throttleVal = 14;
        }
        if (throttleVal > 0)
        {           
            this.GetComponent<Renderer>().enabled = true;
            throttleMaterial.mainTexture = this.throttleTextures[throttleVal - 1];
        }
        else
        {          
            int temp = System.Array.IndexOf(this.throttleTextures, throttleMaterial.mainTexture);
            Debug.Log("Temp value is :" + temp);
            while(temp>0)
            {
                throttleMaterial.mainTexture = this.throttleTextures[temp - 1];
                temp--;
            }
            this.GetComponent<Renderer>().enabled = false;
        }
	}



}