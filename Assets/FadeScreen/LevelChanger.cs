using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class LevelChanger : MonoBehaviour {

	public Animator animator;

	private string levelToLoad;

    private const string SceneName = "Scene_2";
		public Sprite[] background;

    //public GameObject loadingScreen;
   public GameObject panel;
	public Slider slider;

	// Update is called once per frame

/* public void FadeToLevel (string levelName){

  levelToLoad = levelName;
	animator.SetTrigger("FadeOut");


}

public void OnFadeComplete (){

	StartCoroutine(LoadAsynch(levelToLoad));

}

IEnumerator LoadAsynch (string levelName){

	AsyncOperation operation = SceneManager.LoadSceneAsync(levelName);

	loadingScreen.SetActive(true);

	while(!operation.isDone){
		float progress = Mathf.Clamp01(operation.progress / .9f);

	//	Debug.Log(progress);
		slider.value = progress;
		yield return null;
	}
} */
  void Start(){
		panel.SetActive(false);
	}

 public void FadeToLevel (string levelName)
	{
		levelToLoad = levelName;
		//LoadScene.SetActive(true);
		//Text.SetActive(true);
		//animator.SetTrigger("FadeOut");
		panel.SetActive(true);

		if(levelToLoad == "TakeOff_Scene"){
			GameObject.Find("BlackFade").GetComponent<Image>().sprite = background[2];
		}
		if(levelToLoad == "Scene_2"){
			GameObject.Find("BlackFade").GetComponent<Image>().sprite = background[0];
		}
		if(levelToLoad == "floating"){
			GameObject.Find("BlackFade").GetComponent<Image>().sprite = background[0];
		}
		if(levelToLoad == "MainScene"){
			GameObject.Find("BlackFade").GetComponent<Image>().sprite = background[1];
		}
		animator.SetTrigger("FadeOut");
		//animator.SetTrigger("LoadScreen_anim");
        if (GameObject.Find("UniversalSoundObject"))
        {
            AudioSource[] universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
            universalSoundObject[1].Pause();
        }
    }

	public void OnFadeComplete ()
	{
        if(levelToLoad == "Scene_1")
        {
            GameObject vm = GameObject.Find("Voyager Manager");
            if (vm != null)
            {
                ChairInputWrapper cw = vm.GetComponent<ChairInputWrapper>();
                PlayerPrefs.SetFloat("Chair_Yaw", cw.Yaw_chair_angle);
                PlayerPrefs.SetFloat("Chair_Pitch", cw.Pitch_chair_angle);
                Destroy(vm);
            }
            else
            {
                PlayerPrefs.SetFloat("Chair_Yaw", 0);
                PlayerPrefs.SetFloat("Chair_Pitch", 5);
            }

            GameObject api = GameObject.Find("_VoyagerDeviceAPI");
            if (api != null)
                Destroy(api);
        }

        if (levelToLoad == "Scene_2")
        {
					 //GameObject.Find("BlackFade").GetComponent<Image>().Sprite = background[0];

            GameObject vm = GameObject.Find("Voyager Manager");
            if (vm != null)
            {
                ChairInputWrapper cw = vm.GetComponent<ChairInputWrapper>();
                PlayerPrefs.SetFloat("Chair_Yaw", cw.Yaw_chair_angle);
                PlayerPrefs.SetFloat("Chair_Pitch", cw.Pitch_chair_angle);
                Destroy(vm);
            }
            else
            {
                PlayerPrefs.SetFloat("Chair_Yaw", 0);
                PlayerPrefs.SetFloat("Chair_Pitch", 5);
            }

            GameObject api = GameObject.Find("_VoyagerDeviceAPI");
            if (api != null)
                Destroy(api);
        }
        SceneManager.LoadScene(levelToLoad);

	}
}
