﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperFuel_Script : MonoBehaviour
{

    Animator animator;
    bool check = true;
    FMOD.Studio.EventInstance Congrats_Message;
    // Start is called before the first frame update
    void Start()
    {
        Congrats_Message = FMODUnity.RuntimeManager.CreateInstance("event:/SpaceShip/Congrats");
        animator = GetComponent<Animator>();

    }

    IEnumerator levelChangerTimer()
    {
        yield return new WaitForSeconds(10);
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("MainScene");
    }

    // Update is called once per frame
    void Update()
    {
      if(Input.GetKeyDown(KeyCode.Space)){
        animator.SetTrigger("Trigger 1");
        if(check == true)
        {
                //Add narration here
                Congrats_Message.start();
                MainGUI.secondNarration = "Congrats, you have recovered the Concentrated Dark Matter. The Space Quest mission continues...";
                MainGUI.secondNarrationWindow = true;

                check = false;

                StartCoroutine(levelChangerTimer());

                //spaceShip.GetComponent<Ship>().canControl = false;
            } 
      }

    }
}
