﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using System.Threading;

public class MainGUI : MonoBehaviour
{
    public GUISkin mySkin;
    //private Rect windowRect1 = new Rect(Screen.width-950, 160, 500, 380);
    //private Rect windowRect2 = new Rect(Screen.width-1400, 140, 300, 230);

    private Rect windowRect1 = new Rect(Screen.width - Screen.width * 0.65f, Screen.height - Screen.height * 0.97f, Screen.width * 0.30f, Screen.height * 0.50f);
    private Rect windowRect2 = new Rect(Screen.width - Screen.width * 0.98f, Screen.height - Screen.height * 0.85f, Screen.width * 0.25f, Screen.height * 0.4f);

    public bool mainWindow = false;
    public bool narrationWindow = false;
    public static bool gameWindow = false;
    public static bool secondNarrationWindow = false;
    public bool startWindow = false;
    public float waitTime;

    Vector2 scrollPosition;
    public string narration = "";
    public static string secondNarration = "";

    public float health;
    public float distance = 0f;
    public float fuel = 0f;
    public float throttle = 0f;
    //private SpaceShipModel spaceShipModel;

    public static float opacity = 1f;
    Scene m_Scene;
    string sceneName;


    // Use this for initialization
    void Start()
    {
      m_Scene = SceneManager.GetActiveScene();
      sceneName = m_Scene.name;
    }

    void MenuWindow(int windowID)
    {
        GUILayout.BeginHorizontal();
        var style = GUI.skin.GetStyle("label");
        style.fontSize = 40;
        style.fontStyle = FontStyle.Bold;
        GUILayout.Label("SPACE QUEST", style);
        GUILayout.EndHorizontal();

        GUILayout.Space(12);

        var buttonStyle = GUI.skin.GetStyle("button");
        buttonStyle.fontSize = 20;
        buttonStyle.fontStyle = FontStyle.Bold;
        buttonStyle.alignment = TextAnchor.MiddleCenter;
        buttonStyle.normal.textColor = Color.white;

        if (GUILayout.Button("START", buttonStyle, GUILayout.Width(Screen.width*0.25f), GUILayout.Height(Screen.height*0.1f)))
        {
            SceneManager.LoadScene("TakeOff_Scene");
        }

        GUILayout.Space(12);

        if (GUILayout.Button("QUIT", buttonStyle, GUILayout.Width(Screen.width*0.25f), GUILayout.Height(Screen.height*0.1f)))
        {
            //Application.isPlaying = false;
            Application.Quit();
        }

        GUI.DragWindow();
    }

    void NarrationWindow(int windowID)
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true, GUILayout.Width(Screen.width*0.25f), GUILayout.Height(Screen.height*0.33f));
        var style = GUI.skin.GetStyle("label");
        style.fontSize = 27;
        style.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label(narration, style);

        GUILayout.EndScrollView();

        GUI.DragWindow();
    }

    void SecondNarrationWindow(int windowID)
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true, GUILayout.Width(Screen.width * 0.25f), GUILayout.Height(Screen.height * 0.33f));
        var style = GUI.skin.GetStyle("label");
        style.fontSize = 27;
        style.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label(secondNarration, style);

        GUILayout.EndScrollView();

        GUI.DragWindow();
    }


    void GameWindow(int windowID)
    {
        //GUI.color = new Color(GUI.color.r, GUI.color.b, GUI.color.g, .65f * opacity);
        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        GUILayout.BeginVertical();

        var style = GUI.skin.GetStyle("label");
        style.fontSize = 20;
        style.alignment = TextAnchor.MiddleCenter;

        health = PlayerHealth.lives;

        GUILayout.Space(12);

        GUILayout.Label("", "Divider");
        GUILayout.Label("HEALTH: " + health.ToString(), style);
        GUILayout.Label("", "Divider");

        distance = SpaceShipModel.distanceFromDestination;

        if (sceneName == "TakeOff_Scene")
              {
                  GUILayout.Space(8);
                  GUILayout.Label("DISTANCE: " + "6M mi", style);
                  GUILayout.Label("", "Divider");
              }
       else
              {
                  GUILayout.Space(8);
                  GUILayout.Label("DISTANCE: " + Mathf.Round(distance).ToString() + "mi", style);
                  GUILayout.Label("", "Divider");
              }

        fuel = ShipInput.fuel_display;

        GUILayout.Space(12);
        GUILayout.Label("FUEL: " + Mathf.Round(fuel).ToString() + "%", style);
        GUILayout.Label("", "Divider");

        throttle = ShipInput.THROTTLE_SPEED;

        GUILayout.Space(12);
        GUILayout.Label("THROTTLE: " + throttle.ToString() + "x", style);
        GUILayout.Label("", "Divider");

        GUILayout.EndVertical();
        GUILayout.Space(12);
        GUILayout.EndHorizontal();

        GUI.DragWindow();
    }

    /*
    void EndWindow(int windowID)
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true, GUILayout.Width(Screen.width * 0.3f), GUILayout.Height(Screen.height * 0.38f));
        var style = GUI.skin.GetStyle("label");
        style.fontSize = 25;
        style.alignment = TextAnchor.MiddleCenter;
        GUILayout.Label(endNarration, style);
        GUILayout.EndScrollView();

        GUILayout.Space(12);

        var buttonStyle = GUI.skin.GetStyle("button");
        buttonStyle.fontSize = 30;
        buttonStyle.fontStyle = FontStyle.Bold;
        buttonStyle.alignment = TextAnchor.MiddleCenter;
        buttonStyle.normal.textColor = Color.white;

        if (GUILayout.Button("PLAY AGAIN", buttonStyle, GUILayout.Width(Screen.width * 0.3f), GUILayout.Height(Screen.height * 0.1f)))
        {
            SceneManager.LoadScene("MainScene");
        }

        GUILayout.Space(12);

        if (GUILayout.Button("QUIT", buttonStyle, GUILayout.Width(Screen.width * 0.3f), GUILayout.Height(Screen.height * 0.1f)))
        {
            EditorApplication.isPlaying = false;
            Application.Quit();
        }

        GUI.DragWindow();
    }
    */

    /*
    void MyWindow0(int windowID)
    {

        GUILayout.BeginVertical();
        GUILayout.Space(2);

        GUILayout.Label("Standard Label");
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("Short Label", "ShortLabel");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Label("", "Divider");
        GUILayout.Button("Standard Button");

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Button("Short Button", "ShortButton");

        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.Label("", "Divider");
        ToggleBTN = GUILayout.Toggle(ToggleBTN, "This is a Toggle Button");
        GUILayout.Label("", "Divider");
        GUILayout.Box("This is a textbox\n this can be used for big texts");
        GUILayout.TextField("Narrayion");
        HorizSliderValue = GUILayout.HorizontalSlider(HorizSliderValue, 0.0f, 1.0f);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        VertSliderValue = GUILayout.VerticalSlider(VertSliderValue, 0.0f, 1.0f, GUILayout.Height(70f));
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();

        // Make the windows be draggable.
        GUI.DragWindow();
    }

    void MyWindow1(int windowID)
    {

        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("INTERFACE", "ShortLabel");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Space(15);

        GUILayout.Label("", "Divider");

        GUILayout.Label("HEALTH: " + Mathf.Round(health).ToString());

        GUILayout.BeginHorizontal();
        GUILayout.Space(80);
        health = GUILayout.HorizontalSlider(health, 0f, 100f);
        GUILayout.Space(80);
        GUILayout.EndHorizontal();

        GUILayout.Label("", "Divider");

        GUILayout.Space(8);
        GUILayout.Label("ARMOR: " + Mathf.Round(armor).ToString());

        GUILayout.BeginHorizontal();
        GUILayout.Space(80);
        armor = GUILayout.HorizontalSlider(armor, 0f, 100f);
        GUILayout.Space(80);
        GUILayout.EndHorizontal();

        GUILayout.Label("", "Divider");

        bul = bullets * 12f;
        GUILayout.Space(8);
        GUILayout.Label("Weapon: " + bullets.ToString());

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Weapon 1", "ShortButton"))
            bullets = 1f;
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Weapon 2", "ShortButton"))
            bullets = 2f;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Weapon 3", "ShortButton"))
            bullets = 3f;
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Weapon 4", "ShortButton"))
            bullets = 4f;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Label("", "Divider");
        GUILayout.Label("OPACITY: " + (Mathf.Round(opacity * 10f) / 10f).ToString());

        GUILayout.BeginHorizontal();
        GUILayout.Space(80);
        opacity = GUILayout.HorizontalSlider(opacity, 0f, 1f);
        GUILayout.Space(80);
        GUILayout.EndHorizontal();

        GUILayout.Label("", "Divider");

        bullets = (int)bullets;
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        mainWindow = GUILayout.Toggle(mainWindow, "SKIN PREVIEW");

        GUILayout.FlexibleSpace();
        roboGui = GUILayout.Toggle(roboGui, "ROBO-INTERFACE");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.Space(10);
        GUILayout.EndHorizontal();

        GUI.DragWindow();
    }
    */


    IEnumerator ToggleNarrationWindow()
    {
        yield return new WaitForSeconds(waitTime);
        narrationWindow = false;
    }

    IEnumerator ToggleSecondNarrationWindow()
    {
        yield return new WaitForSeconds(5);
        secondNarration = "";
        secondNarrationWindow = false;
    }

    IEnumerator ToggleGameWindow()
    {
        yield return new WaitForSeconds(3);
        gameWindow = false;
    }

    IEnumerator ToggleStartWindow()
    {
        yield return new WaitForSeconds(3);
        startWindow = false;
    }


    // Update is called once per frame
    void OnGUI()
    {
        GUI.skin = mySkin;
        if (mainWindow)
        {
            windowRect1 = GUI.Window(0, windowRect1, MenuWindow, "");
        }

        if (narrationWindow)
        {
            windowRect1 = GUI.Window(1, windowRect1, NarrationWindow, "");
            StartCoroutine(ToggleNarrationWindow());
        }

        if (secondNarrationWindow)
        {
            windowRect1 = GUI.Window(1, windowRect1, SecondNarrationWindow, "");
            StartCoroutine(ToggleSecondNarrationWindow());
        }

        if (gameWindow)
        {
            windowRect2 = GUI.Window(2, windowRect2, GameWindow, "");
            StartCoroutine(ToggleGameWindow());
        }

        if (startWindow)
        {
            windowRect2 = GUI.Window(3, windowRect2, GameWindow, "");
            StartCoroutine(ToggleStartWindow());
        }





    }
}
