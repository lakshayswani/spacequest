﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Positron;
using TMPro;
using System;

public class ChairInputWrapper1 : MonoBehaviour
{
    private ShipInput input;

    Transform prev_tra;
    Transform cur_tra;

    private float target;
    private float throttle;
    private float verticalSpeed;
    private float strafe;

    private float pitch;
    private float yaw;
    private bool hyperSpeed = false;
    private GameObject ship;
    private TextMeshProUGUI text;
    
    //variables for yaw
    private float yaw_chair_angle = 0f;
    private float yaw_prev_ship_angle = 0f;
    private float yaw_curr_ship_angle = 0f;
    private float delta_yaw = 0f;
    private float yaw_vel = 50f;
    private float yaw_acc = 10f;

    //variables for pitch
    private float pitch_chair_angle = 0f;
    private float pitch_prev_ship_angle = 0f;
    private float pitch_curr_ship_angle = 0f;
    private float delta_pitch = 0f;
    private float pitch_vel = 5f;
    private float pitch_acc = 5f;

    float time = 0.0f;

    int count = 0;
    GameObject cp;

    public float Pitch_chair_angle { get => pitch_chair_angle;}
    public float Yaw_chair_angle { get => yaw_chair_angle; }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame

    void Update()
    {
        if (ship == null)
        {
            ship = GameObject.Find("spaceShip");
            cp = GameObject.Find("CameraPosition");
            input = ship.GetComponent<ShipInput>();
            return;
        }

        if(input.CanControl())
        {
            delta_yaw = input.yaw * Time.deltaTime* 10F;
            yaw_chair_angle += delta_yaw;
            ship.transform.Rotate(0,delta_yaw,0);
            Debug.Log(delta_yaw);
        }
    }
    
    void DebugStatements()
    {
        if (text != null)
        {
            
        }
        else
        {
            text = GameObject.Find("Debug Logger").GetComponent<TextMeshProUGUI>();
        }
    }
}
