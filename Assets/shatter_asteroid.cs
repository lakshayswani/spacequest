﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shatter_asteroid : MonoBehaviour
{
    List<GameObject> shatter_asteroids = new List<GameObject>();
    public GameObject smallRock;
    void Start()
    {
        for(int i = 0; i < Random.Range(4,6); i++)
        {
            shatter_asteroids.Add(Instantiate(smallRock, gameObject.transform.position, gameObject.transform.rotation) as GameObject);
        }

        gameObject.GetComponent<MeshRenderer>().enabled = false;

        foreach(GameObject asteroid in shatter_asteroids)
        {
            Rigidbody asteroidRB = asteroid.GetComponent<Rigidbody>();
            asteroid.transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(0, 360), transform.eulerAngles.z);
            float speed = 0.005f;
            //asteroidRB.isKinematic = false;
            Vector3 force = transform.forward;
            force = new Vector3(force.x, 1, force.z);
            asteroidRB.AddForce(force * speed);
            Destroy(asteroid, 100f);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
