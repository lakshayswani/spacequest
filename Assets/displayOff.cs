﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class displayOff : MonoBehaviour
{
    private GameObject spaceShip;
    private MainGUI[] guiComponents;
    private PlayerHealth playerHealth;
    private ScoreBoard scoreBoard;
    private MainGUI narration2;
    public int waitTime = 10;
    private GameObject gui;
    FMODUnity.StudioEventEmitter[] eventEmitter;
    FMODUnity.StudioEventEmitter nextNarrationEvent;

    void Start()
    {
        spaceShip = GameObject.Find("spaceShip");
        gui = GameObject.Find("GUI Camera");
        gui.SetActive(false);
        playerHealth = spaceShip.GetComponent<PlayerHealth>();
        scoreBoard = spaceShip.GetComponent<ScoreBoard>();
        eventEmitter = spaceShip.GetComponents<FMODUnity.StudioEventEmitter>();
        foreach(FMODUnity.StudioEventEmitter emitter in eventEmitter)
        {
            if (emitter.enabled == false)
            {
                nextNarrationEvent = emitter;
            }
        }
        StartCoroutine(StartNextNarration(narration2));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator StartNextNarration(MainGUI narration)
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(waitTime);
        Debug.Log("Turning narration on");
        playerHealth.enabled = true;
        scoreBoard.enabled = true;
        Debug.Log("setting gui true");
        gui.SetActive(true);
        Debug.Log("Done setting gui true");
        nextNarrationEvent.enabled = true;

    }
}
