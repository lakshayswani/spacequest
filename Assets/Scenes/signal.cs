﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class signal : MonoBehaviour
{
    Animator animator;
    // Start is called before the first frame update
    public GameObject cockpitCover;
    void Start()
    {
        cockpitCover = GameObject.Find("cockPitCover");
        animator = GetComponent<Animator>();
        InvokeRepeating("RandomSignal", 3f, 3f);
    }

    public float radius = 3f;
    Vector3 GetPositionAroundObject(Transform tx)
    {
        Vector3 offset = new Vector3(Random.Range(-2f, 2f), 0, Random.Range(0, 1f));
        Vector3 pos = tx.position + offset;
        return pos;
    }

    // Update is called once per frame
    void RandomSignal()
    {
        transform.position = GetPositionAroundObject(cockpitCover.transform);
        animator.SetTrigger("beep");
    }
}
