﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    Animator anim;
    private int prev_coll = 0;
    private GameObject Cylinder;
    private GameObject forcefield;
    private GameObject crashlight;
    private GameObject collisionSound;

    private Vector3 startsize;
    private Vector3 endsize;
    private Vector3 startpos;
    private Vector3 endpos;
    private GameObject canvas;
    private bool ShowGameOver = false;
    private GameObject spaceShip;
    public GameObject explosion;

    private ChairInputWrapper chairWrapper;
    private float pitch_vel = 54f;
    private float pitch_acc = 54f;
    private float yaw_vel = 36f;
    private float yaw_acc = 36f;
 FMOD.Studio.EventInstance Health_over;

    private System.Object[] healthObjects;
    private System.Object[] throttleObjects;
    private Texture[] healthTextures;
    private Texture[] throttleTextures;
    private Material healthMaterial;
    private Material throttleMaterial;
    private float opacity = 1f;
    public static int lives = 5;

    private float endTime = 0f;

    public Material[] healthbar_materials;
    private System.Object[] materialObjects;
    private Material[] materials;

    public GameObject healthBar;

    // Start is called before the first frame update
    void Start()
    {
      Health_over = FMODUnity.RuntimeManager.CreateInstance("event:/SpaceShip/Health_empty");
        forcefield = GameObject.Find("ForceField");
        crashlight = GameObject.Find("CrashLight");
        collisionSound = GameObject.Find("CollisionSound");
        spaceShip = GameObject.Find("spaceShip");

        //anim = Cylinder.GetComponent<Animator>();

        this.healthObjects = Resources.LoadAll("Holographic/output/main/health", typeof(Texture));
        this.healthTextures = new Texture[5];
        int k = 19;
        for (int i = 0; i < 5; i++)
        {
            this.healthTextures[i] = (Texture)this.healthObjects[k];
            k = k - 4;
        }

        // this.materialObjects = Resources.LoadAll("Holographic/output/main/health/Materials", typeof(Material));
        // k = 5;
        // for (int i = 0; i < 5; i++)
        // {
        //     this.materials[i] = (Material)this.materialObjects[k];
        //     k -= 1;
        // }

        Transform children = GameObject.Find("GUI Camera/GUI/health").GetComponentInChildren<Transform>(true);
        foreach (Transform child in children)
        {
            if (child.gameObject.name == "healthbar")
            {
              Debug.Log("Lives"+(healthbar_materials.Length).ToString());
                // child.gameObject.GetComponent<Renderer>().material = healthbar_materials[5-lives];
                healthBar.gameObject.GetComponent<Renderer>().material = healthbar_materials[5-lives];
                healthMaterial =  healthBar.gameObject.GetComponent<Renderer>().material;
                break;
            }
        }


        this.throttleObjects = Resources.LoadAll("Holographic/output/main/armor", typeof(Texture));
        this.throttleTextures = new Texture[3];
        k = 14;
        for (int i = 0; i < 3; i++)
        {
            this.throttleTextures[i] = (Texture)this.throttleObjects[k];
            k = k - 7;
        }

        Transform throttleChildren = GameObject.Find("GUI Camera/GUI/health").GetComponentInChildren<Transform>(true);
        foreach (Transform child in throttleChildren)
        {
            if (child.gameObject.name == "throttle")
            {
                throttleMaterial = child.gameObject.GetComponent<Renderer>().material;
                break;
            }
        }

    }


    void UpdateHealth(int lives)
    {
        Debug.Log("Updating lives");

        // opacity
        healthMaterial.color = new Color(healthMaterial.color.r, healthMaterial.color.b, healthMaterial.color.g, .65f * opacity);
        healthMaterial.mainTexture = healthTextures[lives];
        //throttleMaterial.color = new Color(throttleMaterial.color.r, throttleMaterial.color.b, throttleMaterial.color.g, .65f * opacity);
        //throttleMaterial.mainTexture = throttleTextures[lives];
        MainGUI.gameWindow = true;
        MainGUI.opacity = 1;
    }

    IEnumerator Flicker()
    {
        Debug.Log("Collision: Flicking entered");
        for (int i = 0; i < 2; i++)
        {
            forcefield.GetComponent<MeshRenderer>().enabled = !forcefield.GetComponent<MeshRenderer>().enabled;
            crashlight.GetComponent<Light>().enabled = !crashlight.GetComponent<Light>().enabled;
            yield return new WaitForSeconds(1.0f);
        }
        forcefield.GetComponent<MeshRenderer>().enabled = false;
        Debug.Log("Collision: Flicking End");
        yield break;
    }

    IEnumerator gameover()
    {
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "GameOver")
            {
                canvas = child.gameObject;
                break;
            }
        }
        Debug.Log("reached coroutine");
        canvas.SetActive(true);
        yield return new WaitForSeconds(2.0f);
    }

    void OnCollisionEnter(Collision other)
    {
        Debug.Log("Initial Lives:" + lives);
        if ((other.gameObject.tag == "asteroid" || other.gameObject.tag == "Fireball") && !ShipInput.forceFieldEngaged)
        {
            if (other.gameObject.GetInstanceID() != prev_coll)
            {
                prev_coll = other.gameObject.GetInstanceID();
                Debug.Log("Collision detected");
                collisionSound.GetComponent<AudioSource>().Play();

                if (lives == 5)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    //anim.SetTrigger("hit1");
                    Debug.Log("hit1");
                }
                else if (lives == 4)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    //anim.SetTrigger("hit2");
                    Debug.Log("hit2");
                }
                else if (lives == 3)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    //anim.SetTrigger("hit3");
                    Debug.Log("hit3");
                }
                else if (lives == 2)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    //anim.SetTrigger("hit4");
                    Debug.Log("hit4");
                }
                else if (lives == 1)
                {
                    StartCoroutine(Flicker());
                    lives--;
                    //anim.SetTrigger("hit5");
                    Debug.Log("hit5");
                }
                Debug.Log("New lives:" + lives);

                UpdateHealth(lives);

                if (lives == 0)
                {
                  GameObject.Find("spaceShip").GetComponent<ShipInput>().enabled = false;

                    ShowGameOver = true;
                      Health_over.start();
                    StartCoroutine(gameover());
                    spaceShip.GetComponent<Ship>().canControl = false;

                }




            }
        }
        else if ((other.gameObject.tag == "asteroid" || other.gameObject.tag == "Fireball") && ShipInput.forceFieldEngaged)
        {
            Debug.Log("Collision detected from shield and asteroid");
            Debug.Log(other.gameObject.name);
            Instantiate(explosion, other.gameObject.transform.position, other.gameObject.transform.rotation);
            Destroy(other.gameObject);
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (ShowGameOver)
        {
            endTime += Time.deltaTime;
            if (endTime > 5.0f)
            {
              lives = 5;
                GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("MainScene");
            }

        }

    }


}
