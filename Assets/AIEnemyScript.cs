﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEnemyScript : MonoBehaviour
{
	public float fieldOfViewAngle = 110f;           // Number of degrees, centred on forward, for the enemy see.
	public bool playerInSight;                      // Whether or not the player is currently sighted.
	private AIShoot enemyScript;
	private SphereCollider col;                         // Reference to the Animator.
	private GameObject player;                      // Reference to the player.
    //public LineRenderer laserLineRenderer;
    //public float laserWidth = 1f;

    // Use this for initialization
    void Start()
	{
		enemyScript = this.GetComponent<AIShoot>();
		col = GetComponent<SphereCollider>();
		player = GameObject.FindGameObjectWithTag("Player");
        //laserLineRenderer.SetWidth(laserWidth, laserWidth);
        //enemyScript = this.transform.root.gameObject.GetComponent<Enemy> ();
    }

	// Update is called once per frame
	void Update()
	{

	}
	void OnTriggerStay(Collider other)
	{
	//	Debug.Log("entered trigger stay!");
	//	Debug.Log("other gameobject tag: " + other.gameObject.tag);
	//	Debug.Log("other gameobject name: " + other.gameObject.name);
		if (enemyScript.ShowGameOver == false)
		{
			if (enemyScript.state == AIShoot.State.CheckLastPos)
			{
				fieldOfViewAngle = 180;
			}
			else
			{
				fieldOfViewAngle = 110;
			}
			//Debug.Log("field of view: " + fieldOfViewAngle);
			// If the player has entered the trigger sphere...
			if (other.gameObject.tag == "Player")
			{
				// Create a vector from the enemy to the player and store the angle between it and forward.
				Vector3 direction = other.transform.position - transform.position;
				float angle = Vector3.Angle(direction, transform.forward);
				//Debug.Log("angle: " + angle);
				//Debug.Log("field of view: " + fieldOfViewAngle);
                // If the angle between forward and where the player is, is less than half the angle of view...
                //if (angle < fieldOfViewAngle * 0.5f)
                if (angle < 180)
                {
					RaycastHit hit;
					// ... and if a raycast towards the player hits something...
					if (Physics.Raycast(transform.position + transform.up * 2f, direction.normalized, out hit, col.radius))
					{//added offset so the ray is casted from enemie's head
                     //Debug.DrawLine(transform.position + transform.up * 2f, hit.point, Color.yellow);
                        //laserLineRenderer.enabled = true;
                        //laserLineRenderer.SetPosition(0, transform.position + transform.up * 2f);
                        //laserLineRenderer.SetPosition(1, hit.point);
                        //laserLineRenderer.enabled = true;
                        // ... and if the raycast hits the player...
                        if (hit.collider.gameObject.tag == "Player")
						{
							playerInSight = true;
							//Debug.Log("I SEE U!!!");
						}
						else
						{
							playerInSight = false;
						}
					}
				}
			}
		}
		else
		{
			return;
		}
	}
	void OnTriggerExit(Collider other)
	{
		// If the player leaves the trigger zone...
		if (other.gameObject.tag == "Player")
			// ... the player is not in sight.
			playerInSight = false;
	}
}
