﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    private GameObject Player;
    public bool IsAttacking = false;
    public float fireRate = 0.5f;
    public float nextFire = 0;
    public GameObject shot;
    public Transform shotSpawn;
    // Start is called before the first frame update
    void Awake()
    {
        Player = (GameObject)GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Attacking();
        if (((Player.transform.position - this.transform.position).magnitude < 100f))
        {
            IsAttacking = true;
        }
        else
        {
            IsAttacking = false;
        }
    }
    void Attacking()
    {
        if (IsAttacking)
        {
            //transform.LookAt(Player.transform);
            if (Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                GameObject bolt = Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
                Destroy(bolt, 4);
            }
        }
    }
}
