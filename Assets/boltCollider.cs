﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boltCollider : MonoBehaviour
{
    public GameObject explosion;
    public GameObject spaceship;
    public GameObject asteroid;
    FMOD.Studio.EventInstance ExplosionLow;
    FMOD.Studio.EventInstance ExplosionHigh;
    // Start is called before the first frame update
    void Start()
    {
        spaceship = GameObject.Find("spaceShip");
        asteroid = GameObject.Find("asteroid");
        ExplosionHigh = FMODUnity.RuntimeManager.CreateInstance("event:/SpaceShip/ExplosionHigh");
        ExplosionLow = FMODUnity.RuntimeManager.CreateInstance("event:/SpaceShip/ExplosionLow");
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("bolt hit asteroid name" + collision.gameObject.name + "game object" + collision.gameObject);

        if (collision.gameObject.tag == "asteroid")
        {
            collision.gameObject.GetComponent<shatter_asteroid>().enabled = true;
            //Instantiate(explosion, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            //Destroy(collision.gameObject);
            float dist = Vector3.Distance(spaceship.transform.position, collision.gameObject.transform.position);
            
            if(dist<600)
            {
                //Debug.Log("Nearby" + dist);
                ExplosionHigh.start();
            }
            else
            {
                //Debug.Log("FarAway" +dist);
                ExplosionLow.start();
            }
            Destroy(gameObject, 10f);
            //    //Destroy(explosion, 2.0f);
            //}
            //else if (collision.gameObject.name == "asteroidC(Clone)" || collision.gameObject.name == "asteroidD(Clone)")
            //{
            //    Destroy(gameObject);
            //    GameObject _explosion = Instantiate(explosion, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            //    Destroy(collision.gameObject);
            //    Destroy(_explosion, 10.0f);
            //}
            //else if (collision.gameObject.name == "asteroidE")
            //{
            //    Destroy(gameObject);
            //    GameObject _explosion = Instantiate(explosion, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            //    Destroy(_explosion, 10.0f);
            //}
        }

    }
}
