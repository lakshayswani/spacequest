﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blink : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Blink", 0, 0.7f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    void Blink()
    {
        this.GetComponent<Renderer>().enabled = !this.GetComponent<Renderer>().enabled;
        
    }
}
