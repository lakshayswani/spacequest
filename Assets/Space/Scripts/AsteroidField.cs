﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AsteroidField : MonoBehaviour {
    GameObject spaceShip;
    List<GameObject> asteroid_pool = new List<GameObject>();
    List<Vector3> origialScale = new List<Vector3>();
    List<GameObject> inactive_asteroid_pool = new List<GameObject>();
    [SerializeField]
    GameObject[] asteroids;
    [SerializeField]
    public int asteroidQnt = 9;
    [SerializeField]
    int spacing = 800;
    [SerializeField]
    float activeRadius = 4500;
    [SerializeField]
    public float speed = 0f;
    ScoreBoard sb;
    FMOD.Studio.EventInstance GameEnd;
    private bool firstCall = true;
    private GameObject bgmsound;
    GameObject[] planets;
    GameObject asteroidBelt;
    private bool enableAsteroidBelt = false;
    private GameObject beltPlanet;

    private bool gameDone = false;

    private FirstScene fs;

    // Use this for initialization
    void Start () {
        bgmsound = GameObject.Find("Bgm");
        GenerateField1();
        spaceShip = GameObject.Find("spaceShip");
        GameEnd = FMODUnity.RuntimeManager.CreateInstance("event:/SpaceShip/Rayon_Reached");
        planets = GameObject.FindGameObjectsWithTag("Planet");
        asteroidBelt = GameObject.Find("AsteroidBelt");
        sb = spaceShip.GetComponent<ScoreBoard>();
        fs = GameObject.Find("FirstScene").GetComponent<FirstScene>();
    }

    void Update(){
        UpdateActiveAsteroids();
        ReSpawnInActiveAstroids();
        //SpawnAsteroidBelt();
    }

    void SpawnAsteroidBelt()
    {
        if(enableAsteroidBelt)
        {
            if(!asteroidBelt.active || asteroidBelt.transform.parent==null|| asteroidBelt.transform.parent!= beltPlanet)
            {
                asteroidBelt.transform.parent = beltPlanet.transform;
                asteroidBelt.transform.position = beltPlanet.transform.position;
                asteroidBelt.active = true;

            }
            else
            {
                if (Vector3.Distance(beltPlanet.transform.position, asteroidBelt.transform.position) > 100)
                {
                    asteroidBelt.transform.parent = beltPlanet.transform;
                    asteroidBelt.transform.position = beltPlanet.transform.position;
                }
            }
        }
        else
        {
            asteroidBelt.active = false;
        }


    }
    void ReSpawnInActiveAstroids()
    {
        float minDiff = float.MaxValue;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        Renderer[] renderers;
        foreach (GameObject planet in planets)
        {
            Debug.Log("Planet found");
            renderers = planet.GetComponentsInChildren<Renderer>();
            float diff = Vector3.Distance(planet.transform.position, spaceShip.transform.position);
            if (!GeometryUtility.TestPlanesAABB(planes, renderers[0].bounds)&&diff>50000)
                continue;
            if (diff < minDiff)
            {
                beltPlanet = planet;
                minDiff = diff;
            }
        }
        Debug.Log("Distance=" + minDiff);
        if (minDiff < 35000)
            enableAsteroidBelt = true;
        else
            enableAsteroidBelt = false;
        if (minDiff < 10000)
        {
            Debug.Log("Distance is less than 10000 miles    " + minDiff);
            if (beltPlanet.name == fs.targetPlanet.name && minDiff < 10000-activeRadius-500 && !gameDone)
            {
                gameDone = true;
                //bgmsound.SetActive(false);
                GameEnd.start();
                fs.reachedPlanet = true;
                MainGUI.secondNarration = "You reached the Rayeon planet. Get ready to encounter the enemy. Entering the planet's atmosphere.";
                MainGUI.secondNarrationWindow = true;
            }
            return;
        }
        foreach (GameObject asteroid in inactive_asteroid_pool)
        {
            if(asteroid != null)
            {
                Vector3 temp = Random.onUnitSphere * (activeRadius - 100);
                asteroid.transform.position = temp + spaceShip.transform.position;
                //Vector3 dir = (spaceShip.transform.position - asteroid.transform.position).normalized;
                //asteroid.GetComponent<Rigidbody>().AddForce(dir* speed,ForceMode.Force);
                asteroid.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                asteroid.active = true;
            }
        }

    }

    void GenerateField1()
    {
        for ( int x = 0; x < asteroidQnt; x++)
        {
            SpawnAsteroid1();
        }
    }

    void SpawnAsteroid1()
    {
        GameObject obj = (GameObject)Instantiate(asteroids[Random.Range(0, asteroids.Length)],
            new Vector3(transform.position.x + (Random.Range(0,10) * spacing) + AOffset(),
                transform.position.y + (Random.Range(0, 10) * spacing) + AOffset(),
                transform.position.z + (Random.Range(0, 10) * spacing) + AOffset()), Quaternion.identity, transform);
        //Vector3 towardsSpaceship = transform.position - spaceShip.transform.position;
        //obj.GetComponent<Rigidbody>().velocity = -gameObject.transform.forward * 10f;
        obj.SetActive(false);
        asteroid_pool.Add(obj);
        origialScale.Add(obj.transform.localScale);
    }

    void GenerateField()
    {
        for (int x = 0; x < asteroidQnt; x++)
        {
            for (int y = 0; y < asteroidQnt; y++)
            {
                for (int z = 0; z < asteroidQnt; z++)
                {
                    SpawnAsteroid(x, y, z);
                }
            }
        }
    }
    void SpawnAsteroid(int x, int y, int z) {
        GameObject obj = (GameObject)Instantiate(asteroids[Random.Range(0, asteroids.Length)],
            new Vector3(transform.position.x + (x * spacing) + AOffset(),
                        transform.position.y + (y * spacing) + AOffset(),
                        transform.position.z + (z * spacing) + AOffset()), Quaternion.identity, transform);
        obj.active = false;
        asteroid_pool.Add(obj);
        origialScale.Add(obj.transform.localScale);
    }

    float AOffset() {
       return Random.Range(-spacing * 2, spacing * 2);

    }

    void UpdateActiveAsteroids()
    {

        //Renderer[] renderers;
        //Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        inactive_asteroid_pool.Clear();
        for(int i=0; i< asteroid_pool.Count;i++)
        {
            GameObject asteroid = asteroid_pool[i];
            if(asteroid != null)
            {
                float dis = Vector3.Distance(asteroid.transform.position, spaceShip.transform.position);
                Vector3 dir = (asteroid.transform.position - spaceShip.transform.position).normalized;
                if (Vector3.Dot(dir, spaceShip.transform.forward) < 0)
                {
                    if (dis < activeRadius / 2)
                    {
                        asteroid.SetActive(true);
                        asteroid.transform.localScale = origialScale[i] * (activeRadius - dis) / activeRadius;
                        asteroid.GetComponent<Rigidbody>().AddForce(-1 * spaceShip.transform.forward * speed);
                    }
                    else
                    {
                        asteroid.SetActive(false);
                        inactive_asteroid_pool.Add(asteroid);
                    }
                }
                else
                {
                    if (dis < activeRadius && !firstCall)
                    {
                        asteroid.SetActive(true);
                        asteroid.transform.localScale = origialScale[i] * (activeRadius - dis) / activeRadius;
                        asteroid.GetComponent<Rigidbody>().AddForce(-1 * spaceShip.transform.forward * speed);
                    }
                    else
                    {
                        asteroid.SetActive(false);
                        inactive_asteroid_pool.Add(asteroid);
                    }
                }
            }
        }
        firstCall = false;
        /*
        foreach (GameObject asteroid in asteroid_pool)
        {
            if (Vector3.Distance(asteroid.transform.position, spaceShip.transform.position) < activeRadius)
            {
                asteroid.SetActive(true);
            }
            else
            {
                asteroid.SetActive(false);
                inactive_asteroid_pool.Add(asteroid);
            }
        }*/
    }

    IEnumerator RestartScene()
    {
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene(0);

    }

}
