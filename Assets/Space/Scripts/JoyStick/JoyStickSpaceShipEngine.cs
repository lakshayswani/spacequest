﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyStickSpaceShipEngine : MonoBehaviour
{
    public float enginePitch;
    private float smooth = 0.02f;
    public float maxPitch;
    public float minPitch;
    public bool EngineStarted;
    private AudioSource engineAS;
    // Start is called before the first frame update
    void Start()
    {
        engineAS = this.GetComponent<AudioSource>();
        enginePitch = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (EngineStarted)
        {
            float throttle = Input.GetAxis("RightJoyStickVertical") * -1;
            if (throttle > 0)
            {
                enginePitch = Mathf.Lerp(enginePitch, maxPitch, smooth);
            }
            if (throttle < 0)
            {
                enginePitch = Mathf.Lerp(enginePitch, minPitch, smooth);
            }
            engineAS.pitch = enginePitch;
        }
        else
        {
            engineAS.pitch = 0;
        }
    }
}
