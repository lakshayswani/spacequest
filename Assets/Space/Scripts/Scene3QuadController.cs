﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Scene3QuadController : MonoBehaviour
{
    private GameObject quad;
    private GameObject health;
    private GameObject cylinder;
    //private CylinderHealthBar chb; //= GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>();
    private GameObject forcefield; //= GameObject.Find("ForceField");
    private GameObject crashlight;
    private Animator quadAnim;
    public bool enableAsteroids = false;
    public bool enableControl = false;
    private AudioSource[] soundSource;
    private GameObject ring;
    ScoreBoard sb;

    // Start is called before the first frame update

    public void Start()
    {
        ring = GameObject.Find("FlyerGate");
        ring.SetActive(false);
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Quad")
            {
                quad = child.gameObject;
                break;
            }
        }
        //chb = GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>();
        //forcefield = GameObject.Find("ForceField");
        //crashlight = GameObject.Find("CrashLight");
        quadAnim = quad.GetComponent<Animator>();
        //StartCoroutine(BroadCastInitialMessage());
        soundSource = GameObject.Find("SoundSource").GetComponents<AudioSource>();
        sb = GameObject.Find("spaceShip").GetComponent<ScoreBoard>();
        StartCoroutine(BroadCastInitialMessage());

    }

    public void EnableScene()
    {
        ring.SetActive(true);
        new WaitForSeconds(1f);

        Ship ship = GameObject.Find("spaceShip").GetComponent<Ship>();
        ship.canControl = true;
        sb.startGame();
        if (GameObject.Find("UniversalSoundObject"))
        {
            AudioSource[] universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
            universalSoundObject[1].Play();
        }
    }

    public IEnumerator BroadCastInitialMessage()
    {

        // Initial Message
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        soundSource[0].Play();
        quadAnim.SetTrigger("active");
        soundSource[1].Play();
        yield return new WaitForSeconds(12f);
        soundSource[0].Play();
        yield return new WaitForSeconds(1.3f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);
        EnableScene();
        yield return new WaitForSeconds(1.0f);
    }

}
