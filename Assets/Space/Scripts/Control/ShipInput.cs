﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipInput : MonoBehaviour
{
    [SerializeField]
    private bool usingJoystick = true;
    public bool invertPitch = true;
    public GameObject laser;
    [Range(-1, 1)]
    public float pitch;
    [Range(-1, 1)]
    public float yaw;
    [Range(-1, 1)]
    public float roll;
    [Range(-1, 1)]
    public float strafe;
    //[Range(0, 1)]
    public float throttle;
    [Range(-1, 1)]
    public static float throttle_GUI = 0f;
    public float verticalSpeed;
    public float target;
    public int forceFieldCount = 5;

    public float threshold = 0.1f;
    public float hyperSpeedExtraTrottle = 4f;
    public float rollBankConstant = 0.5f;
    private bool hyperSpeed = false;
    public static bool forceFieldEngaged = false;
    private float nextFire;
    public GameObject shot;
    public Transform shotSpawnR;
    public Transform shotSpawnL;
    private int missileAltCount = 0;
    public Animator xhairAnimator;
    public float fuel = 100f; //Created new variable
    public static float fuel_display = 100f; //To display fuel
    public float fireRate;
    public int fuelBar = 12;
    private GameObject canvas;
     FMOD.Studio.EventInstance FuelEmpty;
    private QuadController qc;

    public bool HyperSpeed()
    {
        return hyperSpeed;
    }

    private bool canControl = false;
    public bool CanControl()
    {
        return canControl;
    }
    // How quickly the throttle reacts to input.
    public static int THROTTLE_SPEED = 1;
    private Ship ship;
    private int multiplyerPitch = 1;


    public int PitchMultiplier()
    {
        return multiplyerPitch;
    }
    private void Awake()
    {
        ship = GetComponent<Ship>();
        //fuel_display = fuel;
    }

    Scene m_Scene;
    string sceneName;
    private void Start()
    {
      FuelEmpty = FMODUnity.RuntimeManager.CreateInstance("event:/SpaceShip/Fuel_empty");
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;
        qc = this.GetComponent<QuadController>();
        GameObject.Find("spaceShip").GetComponent<ShipInput>().enabled = true;


    }

    IEnumerator turnonForceField()
    {
        GameObject.Find("ForceField").GetComponent<MeshRenderer>().enabled = true;
        forceFieldCount--;
        forceFieldEngaged = true;
        yield return new WaitForSeconds(8.0f);
        GameObject.Find("ForceField").GetComponent<MeshRenderer>().enabled = false;
        forceFieldEngaged = false;


    }

    IEnumerator animateCrosshair()
    {
        xhairAnimator.SetBool("isShooting", true);

        yield return new WaitForSeconds(0.1f);
        xhairAnimator.SetBool("isShooting", false);

    }

    IEnumerator delayCanvas()
    {
        yield return new WaitForSeconds(3.0f);
    }

    IEnumerator gameover()
    {
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "GameOver")
            {
                canvas = child.gameObject;
                break;
            }
        }
        Debug.Log("Game over when fuel empty");
        canvas.SetActive(true);
        yield return new WaitForSeconds(2.0f);
    }

    bool fuelReached20 = false;
    bool fuelReached10 = false;
    bool fuelReached5 = false;
    // Update is called once per frame
    void Update()
    {
        if (sceneName == "TakeOff_Scene")
        {
            fuel = 95f;
            fuel_display = 95f;
            Destroy(GameObject.Find("bullet0"));
            bullets.i_initial = 1;
        }
        else if(sceneName == "Scene_2" || sceneName == "floating")
        {
            fuel_display = fuel_display - (0.25f * Time.deltaTime);
            fuel = fuel_display;
            var fuelChange = (int)(Mathf.Ceil(fuel / 8));

            if(fuel < 20 && !fuelReached20){
              StartCoroutine(qc.Fuel20Alert());
              fuelReached20 = true;
            }

            if(fuel < 10 && !fuelReached10){
              StartCoroutine(qc.Fuel10Alert());
              fuelReached10 = true;
            }

            if(fuel < 5 &&  !fuelReached5){
              StartCoroutine(qc.Fuel5Alert());
              fuelReached5 = true;
            }
            Debug.Log("fuelChange = "+ fuelChange);
            if (fuelBar >= 0 && fuelBar != fuelChange)
            {
                fuelBar = fuelChange;
                Destroy(GameObject.Find("bullet" + (12-fuelBar)));
                bullets.i_initial = bullets.i_initial + 1;
            }
        }
        //fuel_display = fuel;
        if(fuel_display <= 0)
        {
          GameObject.Find("spaceShip").GetComponent<ShipInput>().enabled = false;
          FuelEmpty.start();

            StartCoroutine(gameover());
            StartCoroutine(delayCanvas());

            GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("MainScene");
            fuel_display = 100f;
            fuel = 100f;
        }
        canControl = ship.canControl;
        if (invertPitch)
            multiplyerPitch = -1;
        else
            multiplyerPitch = 1;
        if (!usingJoystick)
        {
            //Debug.Log("inside shipinput");
            yaw = Input.GetAxis("Mouse X") * 2;       //Changed to increase the rotation speed of ship while using mouse
            pitch = Input.GetAxis("Mouse Y") * 2;     //Changed to increase the rotation speed of ship while using mouse
            pitch = Mathf.Clamp(pitch, -2.0f, 2.0f);  //Changed to increase the rotation speed of ship while using mouse
            yaw = Mathf.Clamp(yaw, -2.0f, 2.0f);      //Changed to increase the rotation speed of ship while using mouse
            //SetStickCommandsUsingMouse();
            strafe = Input.GetAxis("Horizontal");
            target = Input.GetAxis("Throttle");    //Changed to increase the speed of ship while using keyboard
            if(sceneName == "floating")
            {
                target = target * 2;
            }
            if (Input.GetKey(KeyCode.C))
                verticalSpeed = 1;
            else if (Input.GetKey(KeyCode.V))
                verticalSpeed = -1;
            else
                verticalSpeed = 0;
            if (Input.GetKey(KeyCode.Tab))
                hyperSpeed = true;
            else
                hyperSpeed = false;
            if (Input.GetKeyDown("m") && Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;

                StartCoroutine(animateCrosshair());

                GameObject bolt;
                if(missileAltCount++%2==0)
                    bolt = Instantiate(shot, shotSpawnR.position, shotSpawnR.rotation);
                else
                    bolt = Instantiate(shot, shotSpawnL.position, shotSpawnL.rotation);

                try
                {

                    Destroy(bolt, 5f);
                } catch
                {

                }

                //   laser.transform.position.x += 10;
            }

            if (Input.GetKeyDown("f") && !forceFieldEngaged && forceFieldCount > 0)
            {

                StartCoroutine(turnonForceField());
                //   laser.transform.position.x += 10;
            }

        }
        else
        {
            pitch = Input.GetAxis("LeftJoyStickVertical");
            strafe = Input.GetAxis("RightJoyStickHorizontal");
            yaw = Input.GetAxis("LeftJoyStickHorizontal");
            target = Input.GetAxis("RightJoyStickVertical") * -1;

            yaw = Mathf.Abs(yaw) < threshold ? 0 : yaw;
            pitch = Mathf.Abs(pitch) < threshold ? 0 : pitch;
            pitch = Mathf.Clamp(pitch, -1.0f, 1.0f) * multiplyerPitch;
            yaw = Mathf.Clamp(yaw, -1.0f, 1.0f);


            pitch = pitch > 0 ? 1 : (pitch == 0 ? 0 : -1);
            //yaw = yaw > 0 ? 1 : (yaw == 0 ? 0:-1);
            if (Input.GetButton("ShipUp"))
                verticalSpeed = 1;
            else if (Input.GetButton("ShipDown"))
                verticalSpeed = -1;
            else
                verticalSpeed = 0;
            if (Input.GetButton("JoyStickHyperSpeed"))
                hyperSpeed = true;
            else
                hyperSpeed = false;
        }
        roll = yaw * rollBankConstant;
        if (hyperSpeed && throttle > 0.3)
        {
            target += hyperSpeedExtraTrottle;
            THROTTLE_SPEED = 2;
        }
        else if (target == -1 || throttle > 1)
            THROTTLE_SPEED = 3;
        else
            THROTTLE_SPEED = 1;
        throttle = Mathf.MoveTowards(throttle, target, Time.deltaTime * THROTTLE_SPEED);
        throttle = Mathf.Clamp(throttle, 0, 7.0f);
        throttle_GUI = throttle;
        if (Input.GetButton("Fire1") && Time.time > nextFire && (sceneName == "Scene_2" || sceneName == "floating"))
        {
            nextFire = Time.time + fireRate;
            //Instantiate(shot, shotSpawn.position, transform.rotation);
            GameObject bolt;
            bolt = Instantiate(shot, shotSpawnR.position, shotSpawnR.rotation);

            try
            {

                 Destroy(bolt, 5f);
            }
            catch
            {

            }
            //   laser.transform.position.x += 10;
        }
        if (Input.GetButton("Fire2") && Time.time > nextFire && (sceneName == "Scene_2" || sceneName == "floating"))
        {
            nextFire = Time.time + fireRate;
            //Instantiate(shot, shotSpawn.position, transform.rotation);
            GameObject bolt;
            bolt = Instantiate(shot, shotSpawnL.position, shotSpawnL.rotation);

            try
            {

                Destroy(bolt, 5f);
            }
            catch
            {

            }

            //   laser.transform.position.x += 10;
        }

        //if (Input.GetKeyDown("Fire3") && !forceFieldEngaged && forceFieldCount > 0)
        //{

        //    StartCoroutine(turnonForceField());
        //    //   laser.transform.position.x += 10;
        //}
    }


    private void SetStickCommandsUsingMouse()
    {
        Vector3 mousePos = Input.mousePosition;

        // Figure out most position relative to center of screen.
        // (0, 0) is center, (-1, -1) is bottom left, (1, 1) is top right.
        pitch = (mousePos.y - (Screen.height * 0.5f)) / (Screen.height * 0.5f);
        yaw = (mousePos.x - (Screen.width * 0.5f)) / (Screen.width * 0.5f);

        // Make sure the values don't exceed limits.
        pitch = -Mathf.Clamp(pitch, -1.0f, 1.0f);
        yaw = Mathf.Clamp(yaw, -1.0f, 1.0f);
    }
    private void UpdateKeyboardThrottle(KeyCode increaseKey, KeyCode decreaseKey)
    {
        float target = throttle;

        if (Input.GetKey(increaseKey))
            target = 1.0f;
        else if (Input.GetKey(decreaseKey))
            target = 0.0f;

        throttle = Mathf.MoveTowards(throttle, target, Time.deltaTime * THROTTLE_SPEED);
        throttle_GUI = throttle;
    }

    public void GetInputs(ref float target, ref float throttle, ref float verticalSpeed, ref float strafe, ref float pitch, ref float yaw, ref bool hyperSpeed)
    {
        target = this.target;
        throttle = this.throttle;
        verticalSpeed = this.verticalSpeed;
        strafe = this.strafe;
        pitch = this.pitch;
        yaw = this.yaw;
        hyperSpeed = this.hyperSpeed;
    }
}
