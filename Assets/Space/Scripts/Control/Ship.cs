﻿using UnityEngine;

/// <summary>
/// Ties all the primary ship components together.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ShipPhysics))]
[RequireComponent(typeof(ShipInput))]
public class Ship : MonoBehaviour
{   
    private ShipInput input;
    private ShipPhysics physics;
    private ChairInputWrapper chairWrapper;
    public bool canControl = false;

    // Getters for external objects to reference things like input.
    public Vector3 Velocity { get { return physics.Rigidbody.velocity; } }
    public float Throttle { get { return input.throttle; } }

    private void Awake()
    {
        input = GetComponent<ShipInput>();
        physics = GetComponent<ShipPhysics>();
        GameObject vm = GameObject.Find("Voyager Manager");
        if(vm != null)
        {
            chairWrapper = vm.GetComponent<ChairInputWrapper>();
            chairWrapper.ConvertedRotation = chairWrapper.ConvertQuant2Euler(transform.rotation);
            chairWrapper.Yaw_prev_angle = chairWrapper.ConvertedRotation.y;
            chairWrapper.Pitch_factor = 5f;
        }
    }

    public static float throttle=0;
    public bool clearDeltaOnDisableControl = false;
    private void Update()
    {
        // Pass the input to the physics to move the ship.
        if (canControl)
        {
            clearDeltaOnDisableControl = true;
            if (chairWrapper != null)
            {
                chairWrapper.ConvertedRotation = chairWrapper.ConvertQuant2Euler(transform.rotation);


                if(chairWrapper.Pitch_chair_angle >= 15 )
                {
                    input.pitch = Mathf.Clamp(input.pitch, 0, 1);
                }
                else if (chairWrapper.Pitch_chair_angle <= -5 )
                {
                    input.pitch = Mathf.Clamp(input.pitch, -1, 0);
                }
                chairWrapper.changeChairPitch();
                chairWrapper.changeChairYaw();
                //chairWrapper.DebugStatements(this.gameObject);
            }
            physics.SetPhysicsInput(new Vector3(input.strafe, input.verticalSpeed, input.throttle), new Vector3(input.pitch, input.yaw, input.roll));
        }
        else
        {
            physics.SetPhysicsInput(Vector3.zero, Vector3.zero);
            if (chairWrapper != null && clearDeltaOnDisableControl)
            {
                chairWrapper.Delta_pitch = 0;
                chairWrapper.Delta_yaw = 0;
                clearDeltaOnDisableControl = false;
            }
        }
    }
}
