﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroids : MonoBehaviour
{
    GameObject spaceShip;
    List<GameObject> asteroid_pool = new List<GameObject>();
    List<Vector3> origialScale = new List<Vector3>();
    List<GameObject> inactive_asteroid_pool = new List<GameObject>();
    [SerializeField]
    GameObject[] asteroids;
    [SerializeField]
    int asteroidQnt = 10;
    [SerializeField]
    float activeRadius = 2000;

    int spacing = 800;
    private bool firstCall = true;

    GameObject[] planets;
    // Use this for initialization
    void Start()
    {
        GenerateField();
        spaceShip = GameObject.Find("spaceShip");
        planets = GameObject.FindGameObjectsWithTag("Planet");
    }

    void Update()
    {
        UpdateActiveAsteroids();
        ReSpawnInActiveAstroids();
    }

    void ReSpawnInActiveAstroids()
    {
        float minDiff = float.MaxValue;
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        Renderer[] renderers;
        foreach (GameObject planet in planets)
        {
            renderers = planet.GetComponentsInChildren<Renderer>();
            float diff = Vector3.Distance(planet.transform.position, spaceShip.transform.position);
            if (!GeometryUtility.TestPlanesAABB(planes, renderers[0].bounds) && diff > 10000)
                continue;
            if (diff < minDiff)
            {
                minDiff = diff;
            }
        }
        if (minDiff < 10000)
            return;

        foreach (GameObject asteroid in inactive_asteroid_pool)
        {
            Vector3 temp = Random.onUnitSphere * (activeRadius - 100);
            asteroid.transform.position = temp + spaceShip.transform.position;
            asteroid.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            asteroid.SetActive(true);
        }
    }

    void GenerateField()
    {
        for (int x = 0; x < asteroidQnt; x++)
        {
            for (int y = 0; y < asteroidQnt; y++)
            {
                for (int z = 0; z < asteroidQnt; z++)
                {
                    SpawnAsteroid(x, y, z);
                }
            }
        }
    }
    void SpawnAsteroid(int x, int y, int z)
    {
        GameObject obj = (GameObject)Instantiate(asteroids[Random.Range(0, asteroids.Length)],
            new Vector3(transform.position.x + (x * spacing) + AOffset(),
                        transform.position.y + (y * spacing) + AOffset(),
                        transform.position.z + (z * spacing) + AOffset()), Quaternion.identity, transform);
        obj.SetActive(false);
        asteroid_pool.Add(obj);
        origialScale.Add(obj.transform.localScale);
    }

    float AOffset()
    {
        return Random.Range(-spacing * 2, spacing * 2);
    }

    void UpdateActiveAsteroids()
    {
        inactive_asteroid_pool.Clear();
        for (int i = 0; i < asteroid_pool.Count; i++)
        {
            GameObject asteroid = asteroid_pool[i];
            float dis = Vector3.Distance(asteroid.transform.position, spaceShip.transform.position);
            if (dis < activeRadius && !firstCall)
            {
                asteroid.SetActive(true);
                asteroid.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(Random.Range(0, 1000), Random.Range(0, 1000), Random.Range(0, 1000)));
                asteroid.transform.localScale = origialScale[i] * (activeRadius - dis) / activeRadius;
            }
            else
            {
                asteroid.SetActive(false);
                inactive_asteroid_pool.Add(asteroid);
            }
        }
        firstCall = false;
    }
}
