using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]
    public class CarUserControl : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use
        public bool canControl;
        private ChairInputWrapper chairWrapper;

        private GameObject cameraRig;
        public bool clearDeltaOnDisableControl = false;

        private void Start()
        {
            cameraRig = GameObject.Find("CameraRig");
        }
        private void Awake()
        {
            // get the car controller
            GameObject vm = GameObject.Find("Voyager Manager");
            if (vm != null)
            {
                chairWrapper = vm.GetComponent<ChairInputWrapper>();
                chairWrapper.Yaw_factor = 2f;
                chairWrapper.ConvertedRotation = chairWrapper.ConvertQuant2Euler(transform.rotation);
                chairWrapper.Pitch_prev_angle = chairWrapper.ConvertedRotation.x;
                chairWrapper.Yaw_prev_angle = chairWrapper.ConvertedRotation.y;

                chairWrapper.Pitch_factor = -2.5f;
            }
            m_Car = GetComponent<CarController>();
        }


        private void FixedUpdate()
        {
            if (canControl)
            {
                
                // pass the input to the car!
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");

                float handbrake = CrossPlatformInputManager.GetAxis("Jump");
                if (chairWrapper != null)
                {
                    clearDeltaOnDisableControl = true;
                    chairWrapper.ConvertedRotation = chairWrapper.ConvertQuant2Euler(transform.rotation);
                    Vector3 temp = chairWrapper.ConvertQuant2Euler(cameraRig.transform.rotation);
                    float delta_correction_z = chairWrapper.ConvertedRotation.z - temp.z;
                    cameraRig.transform.Rotate(0, 0, delta_correction_z);

                    chairWrapper.changeChairPitch();
                    //chairWrapper.changeChairYaw();
                    chairWrapper.changeChairYaw1(this.gameObject);
                    //chairWrapper.DebugStatements(this.gameObject);
                }
                m_Car.Move(h, v, v, handbrake);

            }
            else
            {
                m_Car.Move(0, 0, 0, 1);
                if (chairWrapper != null && clearDeltaOnDisableControl)
                {
                    chairWrapper.Delta_pitch = 0;
                    chairWrapper.Delta_yaw = 0;
                    clearDeltaOnDisableControl = false;
                }
            }
        }
    }
}
