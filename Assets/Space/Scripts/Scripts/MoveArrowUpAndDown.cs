﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveArrowUpAndDown : MonoBehaviour
{
    // Start is called before the first frame update
    int count = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 1);
        if(count < 100)
            transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(0, 5, 0), 0.004f );
        else
            transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(0, -5, 0), 0.004f);
        count = (count + 1) % 200;
    }
}
