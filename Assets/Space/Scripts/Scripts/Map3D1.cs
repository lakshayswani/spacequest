﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map3D1 : MonoBehaviour
{
    List<GameObject> planets = new List<GameObject>();
    //GameObject[] planets;
    GameObject sun;
    GameObject map;
    GameObject spaceShip;
    GameObject miniSun;
    GameObject miniShip;
    Dictionary<string,GameObject> myMap = new Dictionary<string, GameObject>();
    Vector3 oldPosition;
    Vector3 newPosition;
    public float maxDistance = 100000f;

    public bool displayMap = true;

    private float temp;
    // Start is called before the first frame update
    void Start()
    {
        planets.AddRange(GameObject.FindGameObjectsWithTag("Planet"));
        sun = GameObject.Find("sun");
        
        map = GameObject.Find("Map");
        spaceShip = GameObject.Find("spaceShip");

        miniSun = (GameObject)Instantiate(Resources.Load("Sun_Map"), new Vector3(0, 0, 0), Quaternion.identity); ;
        miniShip = (GameObject) Instantiate(Resources.Load("WaspInterjector_prefab"), new Vector3(0, 0, 0), Quaternion.identity);

        //miniSun.transform.GetChild(1).GetComponent<Light>().enabled = false;

        InstatiateEverything();
        oldPosition = spaceShip.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        temp = Input.GetAxis("MapButton");
        if (temp == 1f)
            displayMap = true;
        else if(temp == -1f)
            displayMap = false;
        if (displayMap)
        {
            map.active = true;
            UpdatePlanetPositions();
        }
        else
        {
            map.active = false;
        }
    }

    void InstatiateEverything()
    {
        miniShip.transform.localScale = new Vector3(0.002f, 0.002f, 0.002f);
        miniShip.transform.parent = map.transform;
        miniShip.transform.position = map.transform.position;
        miniShip.transform.rotation = spaceShip.transform.rotation;

        miniSun.transform.parent = map.transform;
        myMap.Add(sun.name, miniSun);
        Vector3 newVector = ((spaceShip.transform.position - sun.transform.position).normalized);
        newVector *= 0.2f;
        miniSun.transform.position = miniShip.transform.position - newVector;

        /*miniSun.transform.localScale = new Vector3(0.0001f, 0.0001f, 0.0001f);
        miniSun.transform.parent = map.transform;
        myMap.Add(sun.name,miniSun);
        Vector3 newVector = ((spaceShip.transform.position - sun.transform.position).normalized);
        newVector *= 0.2f;
        miniSun.transform.position = miniShip.transform.position - newVector;
        miniSun.GetComponent<RotateAround>().enabled = false;
        ParticleSystem ps = miniSun.GetComponent<ParticleSystem>();
        ps.startSize = 0.1f;
        ps.simulationSpace = ParticleSystemSimulationSpace.Local;
        var sh = ps.shape;
        sh.radius = 1f;*/

        /*miniSun.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        miniSun.transform.parent = map.transform;
        miniSun.transform.position = map.transform.position;
        miniSun.GetComponent<RotateAround>().enabled = false;
        ParticleSystem ps = miniSun.GetComponent<ParticleSystem>();
        ps.startSize = 0.1f;
        ps.simulationSpace = ParticleSystemSimulationSpace.Local;
        var sh = ps.shape;
        sh.radius = 1f;

        miniShip.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        miniShip.transform.parent = map.transform;
        miniShip.transform.position = miniShip.transform.position;
        Quaternion temp = miniShip.transform.localRotation;
        temp.y = 0;
        miniShip.transform.localRotation = temp;
        if (miniShip.GetComponent<Ship>() != null)
            miniShip.GetComponent<Ship>().enabled = false;
        if (miniShip.GetComponent<ShipInput>() != null)
            miniShip.GetComponent<ShipInput>().enabled = false;
        if (miniShip.GetComponent<ShipPhysics>() != null)
            miniShip.GetComponent<ShipPhysics>().enabled = false;
        if(miniShip.GetComponent<Map3D>() != null)
            miniShip.GetComponent<Map3D>().enabled = false;
        if (miniShip.GetComponent<Rigidbody>() != null)
            miniShip.GetComponent<Rigidbody>().isKinematic = false;
        //miniShip.transform.position = miniSun.transform.position + new Vector3(-0.1f, 0, 0);
        UpdateSpaceShipPositionOnMap();
        */
        InstantiatePlanets();
        planets.Add(sun);
    }

    void InstantiatePlanets()
    {
        foreach(GameObject planet in planets)
        {
            //Debug.Log(planet.name + "_Map");
            GameObject miniPlanet = (GameObject)Instantiate(Resources.Load(planet.name+"_Map"), new Vector3(0, 0, 0), Quaternion.identity);
            //float dist = Vector3.Distance(planet.transform.position, sun.transform.position)/maxDistance;
            //GameObject miniPlanet = Instantiate(planet, new Vector3(0, 0, 0), Quaternion.identity);
            /*if(planet.name == "OrangePlanet")
            {
                miniPlanet.AddComponent<blink>();
            }*/

            //miniPlanet.transform.localScale = new Vector3(0.0005f, 0.0005f, 0.0005f);
            miniPlanet.transform.parent = map.transform;
            miniPlanet.transform.tag = "Untagged";
            if (miniPlanet.GetComponent<RotateAround>() != null)
                miniPlanet.GetComponent<RotateAround>().enabled = false;
            if (miniPlanet.GetComponent<RadarTarget>() != null)
                miniPlanet.GetComponent<RadarTarget>().enabled = false;
            Vector3 newVector = ((spaceShip.transform.position - planet.transform.position).normalized);
            //dist *= -0.3f;
            //newVector *= dist;
            newVector *= 0.1f;
            miniPlanet.transform.position = miniShip.transform.position - newVector;
            myMap.Add(planet.name,miniPlanet);
        }
    }

    void UpdatePlanetPositions()
    {
        foreach(GameObject planet in planets)
        {
            //Debug.Log("spaceship position " + spaceShip.transform.position + "planet position " + planet.transform.position);
            float dist = ((Vector3.Distance(spaceShip.transform.position, planet.transform.position) + 4000) / maxDistance)/4;
            //Debug.Log(planet.name+","+ dist);
            GameObject miniPlanet = myMap[planet.name];
            Vector3 newVector = ((spaceShip.transform.position - planet.transform.position).normalized);
            //newVector *= 0.1f;
            dist = dist > 0.2f ? 0.2f : dist;
            newVector *= dist;
            miniPlanet.transform.position = miniShip.transform.position - newVector;
        }
        //newPosition = spaceShip.transform.position;
        //Debug.Log(spaceShip.transform.position +":"+((newPosition - oldPosition)/0.0));

        //miniShip.transform.position += (newPosition - oldPosition).normalized * 0.00001f;
        //oldPosition = newPosition;
    }
}
