﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveTowardsTarget : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target;
    ParticleSystem smoke;
    float initialRate = 10.76f;

    void Start()
    {
        smoke = GameObject.Find("WhiteSmoke").GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if(target!=null)
        { 
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, 5 * Time.deltaTime);
            if(Vector3.Distance(transform.position, target.transform.position) < 2)
            {
    #pragma warning disable CS0618 // Type or member is obsolete
                smoke.maxParticles -= 1000;
    #pragma warning restore CS0618 // Type or member is obsolete
                //em.rateOverTime = em.rateOverTime - new ParticleSystem.MinMaxCurve(0.0f, initialRate / 6);
                Destroy(gameObject);
            }
        }
    }
}
