﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mech_Holo_Parts : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject bip;
    Vector3 prev_height;
    Vector3 current_height;
    void Start()
    {
        bip = GameObject.Find("Bip001");
        prev_height = current_height = bip.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        current_height = bip.transform.localPosition;
        transform.localPosition += current_height-prev_height;
        prev_height = current_height;
    }
}
