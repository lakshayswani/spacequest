﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tutorial_1 : MonoBehaviour
{
    // Start is called before the first frame update
    private ShipInput input;
    private Ship ship;
    private ShipPhysics physics;

    private GameObject spaceShip;

    public int tutCount = 0;

    private GameObject ring;

    Vector3 initialPosition;

    private GameObject quad;
    private Animator quadAnim;
    private GameObject display1;
    private GameObject display2;
    private GameObject display3;

    private float defaultLinearForce;
    private float defaultAngularForce;

    private Vector3 defaultLinearForceVect;
    private Vector3 defaultAngularForceVect;

    private bool resetThirdRing = false;
    private bool resetFourthRing = false;

    private bool completedTutorial = false;

    void Start()
    {

        ring = GameObject.Find("FlyerGate");
        ring.SetActive(false);
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Quad")
            {
                quad = child.gameObject;
                break;
            }
        }

        foreach (Transform child in quad.GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "InitialMessage")
            {
                display1 = child.gameObject;
            }

            if (child.gameObject.name == "Autopilot")
            {
                display2 = child.gameObject;
            }

            if (child.gameObject.name == "ForceField")
            {
                display3 = child.gameObject;
            }
        }

        quadAnim = quad.GetComponent<Animator>();
        spaceShip = GameObject.Find("spaceShip");
        input = spaceShip.GetComponent<ShipInput>();
        ship = spaceShip.GetComponent<Ship>();
        physics = spaceShip.GetComponent<ShipPhysics>();
        defaultLinearForce = physics.LinearForceMultiplier;
        defaultAngularForce = physics.angularForceMultiplier;

        defaultLinearForceVect = physics.linearForce;
        defaultAngularForceVect = physics.angularForce;

        if (GameObject.Find("UniversalSoundObject"))
        {
            AudioSource[] universalSoundObject = GameObject.Find("UniversalSoundObject").GetComponents<AudioSource>();
            universalSoundObject[1].Play();
        }


    }

    // Update is called once per frame
    void Update()
    {
        if (tutCount == 0)
        {
            Debug.Log(tutCount);
            StartCoroutine(DisplayInitialMessage());
            tutCount = 1;
        }
        if (tutCount == 2)
        {
            physics.LinearForceMultiplier = 25;
            physics.linearForce = new Vector3(0, 0, 5f);
            physics.angularForceMultiplier = 0;
            physics.angularForce = Vector3.zero;
            ship.canControl = true;
            tutCount++;
        }
        if (tutCount == 4)
        {
            ship.canControl = false;
            StartCoroutine(CompletedFirstRing());
            tutCount++;
        }

        if (tutCount == 6)
        {
            ship.canControl = true;
            physics.linearForce = new Vector3(3f, 0, 0f);
            tutCount++;
        }
        if (tutCount == 8)
        {
            ship.canControl = false;
            StartCoroutine(CompletedSecondRing());
            tutCount++;
        }
        if (tutCount == 10)
        {
            ship.canControl = true;
            physics.linearForce = new Vector3(0, 0, 2f);
            physics.LinearForceMultiplier = defaultLinearForce;
            physics.angularForceMultiplier = defaultAngularForce;
            physics.angularForce = new Vector3(defaultAngularForceVect.x, 0f, 0f);
            tutCount++;
        }

        if (tutCount == 11) //check if we missed the ring
        {
            if (ring.transform.position.x + 50f < spaceShip.transform.position.x)
            {
                resetThirdRing = true;
            }
        }

        if (tutCount == 12)
        {
            ship.canControl = false;
            StartCoroutine(CompletedThirdRing());
            tutCount++;
        }

        if (tutCount == 14)
        {
            ship.canControl = true;
            physics.linearForce = new Vector3(0, 0, 2f);
            physics.angularForceMultiplier = defaultAngularForce;
            physics.angularForce = new Vector3(0f, defaultAngularForceVect.y, 0f);
            tutCount++;
        }

        if (tutCount == 15)
        {
            if (ring.transform.position.x + 50f < spaceShip.transform.position.x)
            {
                resetFourthRing = true;
            }
        }

        /*if (tutCount == 16)
        {
            completedTutorial = true;
        }*/

        if(tutCount == 16)
        {
            ship.canControl = false;
            StartCoroutine(StartHyperSpeed());
            tutCount++;
        }

        if(tutCount == 18)
        {
            ship.canControl = true;
            physics.linearForce = new Vector3(0, 0, 5f);
            physics.angularForceMultiplier = defaultAngularForce;
            physics.angularForce = new Vector3(0f, 0f, 0f);
            tutCount++;
        }

        if(tutCount == 20)
        {
            completedTutorial = true;
            physics.linearForce = defaultLinearForceVect;
            physics.LinearForceMultiplier = defaultLinearForce;
            physics.angularForce = defaultAngularForceVect;
            physics.angularForceMultiplier = defaultAngularForce;
            tutCount++;
        }

        if (resetThirdRing)
        {
            //ship.canControl = false;
            StartCoroutine(ResetThirdRing());
            resetThirdRing = false;
        }
        if(resetFourthRing)
        {
            StartCoroutine(ResetFourthRing());
            resetFourthRing = false;
        }

        if(completedTutorial)
        {
            StartCoroutine(CompletedTutorial());
            completedTutorial = false;
        }

    }

    void Disable(GameObject go)
    {
        Destroy(go);
    }

    IEnumerator StartHyperSpeed()
    {
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Woah you mastered the left joystick!      " +
            "\nNow let's go in hyper speed!";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(5.0f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        ring.SetActive(true);
        ring.transform.position = spaceShip.transform.position + spaceShip.transform.forward * 2000f;
        ring.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Move your right joystick forward and press the trigger to go in hyperspeed!";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(5.5f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        

        tutCount++;
    }


    IEnumerator CompletedFirstRing()
    {
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Good job..   \nNow the ring is to your left..    ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(3.0f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        ring.transform.position = spaceShip.transform.position + new Vector3(10f, 0f, 100f);
        ring.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Move the right joystick left to strafe left..    ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(3f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        tutCount++;

    }

    IEnumerator ResetThirdRing()
    {
        ring.transform.position = spaceShip.transform.position + spaceShip.transform.forward * 500f + spaceShip.transform.up * 40f; //new Vector3(500f, 40f, 0f);
        ring.transform.localEulerAngles = new Vector3(0f, 90f, 0f);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Looks like you missed the ring...    " +
            "\nMove the left joystick down to pitch up..    ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(5f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        //ship.canControl = true;

    }

    IEnumerator ResetFourthRing()
    {
        ring.transform.position = spaceShip.transform.position + spaceShip.transform.forward * 600f + spaceShip.transform.right * 80f;//new Vector3(500f, 0f, 40f);
        ring.transform.localEulerAngles = spaceShip.transform.localEulerAngles + new Vector3(0f, 75f, 0f);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Looks like you missed the ring...    ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(3f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        //ship.canControl = true;

    }

    IEnumerator CompletedSecondRing()
    {
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Woah nice..   " +
            "\nYou mastered the right joystick!    \n" +
            "Now let's master the left joystick...   ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(7.0f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        //ring.transform.position = spaceShip.transform.position + new Vector3(10f, 0f, 100f);
        //ring.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        ring.transform.position = spaceShip.transform.position + spaceShip.transform.forward * 300f + spaceShip.transform.up * 30f; //new Vector3(300f, 30f, 0f);
        ring.transform.localEulerAngles = new Vector3(0f, 90f, 0f);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Looks like the ring moved again..    ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(2f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Move the left joystick down to pitch up..                " +
            "\nDon't forget to move the spaceship forward";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(6f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        tutCount++;
    }

    IEnumerator CompletedThirdRing()
    {
        ring.transform.position = spaceShip.transform.position + spaceShip.transform.forward * 300f + spaceShip.transform.right * 80f; //new Vector3(300f, 0f, 40f);
        ring.transform.localEulerAngles = spaceShip.transform.localEulerAngles + new Vector3(0f, 75f, 0f);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "You are a natural..   \nThe ring moved to your right";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(5.0f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Move the left joystick right to rotate right..                " +
            "\nDon't forget to move the spaceship forward";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(6f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        tutCount++;
    }

    IEnumerator CompletedRing()
    {
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Good job..   ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(1.5f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);
    }

    IEnumerator CompletedTutorial()
    {
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Looks like I don't have anything more to teach you...";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(4.2f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Congrats on completing the tutorial!         \n" +
            "You are ready for anything!";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(8f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_1");
    }

    IEnumerator DisplayInitialMessage()
    {
        
        quad.SetActive(true);
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Welcome to the Tutorial!                    " +
            "\nHere we will teach you how to use the joysticks to control the vehicles.               ";
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(8.5f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        quad.SetActive(true);
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Each vehicle has two joysticks...";
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(3f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);


        quad.SetActive(true);
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "The left joystick is used to control the rotations...       ";
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(4f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        quad.SetActive(true);
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "The right joystick is used to move the vehicles....       ";
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(3.5f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);

        ring.SetActive(true);
        
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Now you'll see a ring apearing infront of you...    ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(3.5f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);
        

        ring.SetActive(true);
        display1.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Move the right joystick forward to move the spaceship forward..    ";
        quad.SetActive(true);
        quadAnim.SetTrigger("incoming");
        yield return new WaitForSeconds(1.0f);
        quadAnim.SetTrigger("active");        StartCoroutine(display1.transform.GetChild(0).GetComponent<TeleType>().Type());
        yield return new WaitForSeconds(4f);
        quadAnim.SetTrigger("closing");
        yield return new WaitForSeconds(1.0f);
        quad.SetActive(false);



        tutCount = 2;
    }
}
