﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map3D : MonoBehaviour
{
    GameObject[] planets;
    GameObject sun;
    GameObject map;
    GameObject spaceShip;
    GameObject miniSun;
    GameObject miniShip;

    public float maxDistance = 100000f;

    public bool displayMap = true;

    private float temp;
    // Start is called before the first frame update
    void Start()
    {
        planets = GameObject.FindGameObjectsWithTag("Planet");
        sun = GameObject.Find("sun");
        map = GameObject.Find("Map");
        spaceShip = GameObject.Find("spaceShip");

        miniSun = Instantiate(sun, new Vector3(0, 0, 0), Quaternion.identity);
        miniShip = (GameObject) Instantiate(Resources.Load("WaspInterjector_prefab"), new Vector3(0, 0, 0), Quaternion.identity);

        miniSun.transform.GetChild(1).GetComponent<Light>().enabled = false;

        InstatiateEverything();
    }

    // Update is called once per frame
    void Update()
    {
        temp = Input.GetAxis("MapButton");
        if (temp == 1f)
            displayMap = true;
        else if(temp == -1f)
            displayMap = false;
        if (displayMap)
        {
            map.active = true;
            UpdateSpaceShipPositionOnMap();
        }
        else
        {
            map.active = false;
        }
    }

    void InstatiateEverything()
    {
        miniSun.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        miniSun.transform.parent = map.transform;
        miniSun.transform.position = map.transform.position;
        miniSun.GetComponent<RotateAround>().enabled = false;
        ParticleSystem ps = miniSun.GetComponent<ParticleSystem>();
        ps.startSize = 0.1f;
        ps.simulationSpace = ParticleSystemSimulationSpace.Local;
        var sh = ps.shape;
        sh.radius = 1f;
        /*
        miniShip.transform.localScale = new Vector3(0.001f, 0.001f, 0.001f);
        miniShip.transform.parent = map.transform;
        miniShip.transform.position = miniShip.transform.position;
        Quaternion temp = miniShip.transform.localRotation;
        temp.y = 0;
        miniShip.transform.localRotation = temp;
        if (miniShip.GetComponent<Ship>() != null)
            miniShip.GetComponent<Ship>().enabled = false;
        if (miniShip.GetComponent<ShipInput>() != null)
            miniShip.GetComponent<ShipInput>().enabled = false;
        if (miniShip.GetComponent<ShipPhysics>() != null)
            miniShip.GetComponent<ShipPhysics>().enabled = false;
        if(miniShip.GetComponent<Map3D>() != null)
            miniShip.GetComponent<Map3D>().enabled = false;
        if (miniShip.GetComponent<Rigidbody>() != null)
            miniShip.GetComponent<Rigidbody>().isKinematic = false;
        //miniShip.transform.position = miniSun.transform.position + new Vector3(-0.1f, 0, 0);
        UpdateSpaceShipPositionOnMap();
        */
        InstantiatePlanets();
    }

    void InstantiatePlanets()
    {
        foreach(GameObject planet in planets)
        {
            //float dist = Vector3.Distance(planet.transform.position, sun.transform.position)/maxDistance;
            GameObject miniPlanet = Instantiate(planet, new Vector3(0, 0, 0), Quaternion.identity);
            //miniPlanet.transform.localScale = new Vector3(0.0005f, 0.0005f, 0.0005f);
            miniPlanet.transform.parent = map.transform;
            miniPlanet.transform.tag = "Untagged";
            if(miniPlanet.GetComponent<RotateAround>() != null)
                miniPlanet.GetComponent<RotateAround>().enabled = false;
            if (miniPlanet.GetComponent<RadarTarget>() != null)
                miniPlanet.GetComponent<RadarTarget>().enabled = false;
            Vector3 newVector = ((sun.transform.position - planet.transform.position).normalized);
            //dist *= -0.3f;
            //newVector *= dist;
            newVector *= 0.125f;
            miniPlanet.transform.position = miniSun.transform.position - newVector;
        }
    }

    void UpdateSpaceShipPositionOnMap()
    {

        Vector3 newVector = ((sun.transform.position - spaceShip.transform.position).normalized);
        //dist *= -0.03f;
        newVector *= 0.125f;
        //Debug.Log(newVector);
        miniShip.transform.position = miniSun.transform.position - newVector;

    }
}
