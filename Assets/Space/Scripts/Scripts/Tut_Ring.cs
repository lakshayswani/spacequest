﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tut_Ring : MonoBehaviour
{

    Tutorial_1 tut;
    // Start is called before the first frame update
    void Start()
    {
        tut = GameObject.Find("spaceShip").GetComponent<Tutorial_1>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (!other.name.Equals("spaceShip"))
            return;
        tut.tutCount++;
        Debug.Log(tut.tutCount);
    }
}
