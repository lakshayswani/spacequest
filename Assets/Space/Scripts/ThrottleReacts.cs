﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShipInput))]

public class ThrottleReacts : MonoBehaviour
{
    private ShipInput input;
    private ParticleSystem dust;
    private AudioSource audio;
    //GameObject velocityCylinder;
    private float maxScale;
    private float smooth = 0.02f;
    public float enginePitch;
    public float maxPitch;


    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<ShipInput>();
        GameObject dustParticles = GameObject.Find("ThrottleDust");
        if(dustParticles!=null)
        {
            dust = dustParticles.GetComponent<ParticleSystem>();
        }
        audio = GetComponent<AudioSource>();
        //velocityCylinder = GameObject.Find("Velocity");
        //maxScale = velocityCylinder.transform.localScale.y;
        //Vector3 temp = velocityCylinder.transform.localScale;
        //temp.y = 0;
        //velocityCylinder.transform.localScale = temp;
        enginePitch = 1;
    }

    // Update is called once per frame
    void Update()
    {
        float throttle = input.throttle;
#pragma warning disable CS0618 // Type or member is obsolete
        if (throttle == 0)
        {
            if (dust != null)
            {
                dust.GetComponent<ParticleSystemRenderer>().lengthScale = 0;
                dust.maxParticles = 0;
            }
        }
        else if (throttle <= 1.0f)
        {
            if (dust != null)
            {
                dust.maxParticles = 50;
                dust.GetComponent<ParticleSystemRenderer>().lengthScale = throttle * 200.0f;
            }
            //changeVelocityScale(throttle);
            audio.pitch = 1f+(throttle * (.5f));
            //enginePitch = Mathf.Lerp(enginePitch, maxPitch, smooth);

        }
        else
        {
            if (dust != null)
            { 
                dust.maxParticles = 5000;
                dust.GetComponent<ParticleSystemRenderer>().lengthScale = Mathf.Max(200.0f, throttle*150f);
            }
            audio.pitch = (throttle * (3f)/5f);
            //enginePitch = Mathf.Lerp(enginePitch, maxPitch, smooth);
            //audio.pitch = enginePitch;
        }
#pragma warning restore CS0618 // Type or member is obsolete
    }

    /*
    void changeVelocityScale(float throttle)
    {

        Vector3 temp = velocityCylinder.transform.localScale;
        temp.y = throttle * maxScale;
        float diff = temp.y - velocityCylinder.transform.localScale.y;
        velocityCylinder.transform.localPosition += new Vector3(0f, diff, 0f);
        velocityCylinder.transform.localScale = temp;
    }*/
}
