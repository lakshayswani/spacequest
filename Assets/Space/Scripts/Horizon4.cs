﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horizon4 : MonoBehaviour
{
    public Material[] skyboxes;
    public GameObject sphere;
    private GameObject horizon;
    private GameObject newEarth;
    private GameObject scene;
    private GameObject light;

    void Start() {
        horizon = GameObject.Find("horizon");
        horizon.gameObject.SetActive(true);
        scene = GameObject.Find("SunRotation");
        scene.gameObject.SetActive(true);
        light = GameObject.Find("spacelight");
        light.gameObject.SetActive(false);
        newEarth = GameObject.Find("earth");
        newEarth.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Object entered the trigger!");

        RenderSettings.skybox = skyboxes[0];
        sphere.SetActive(true);
		horizon.gameObject.SetActive(false);
		scene.gameObject.SetActive(false);
		newEarth.gameObject.SetActive(true);
		light.gameObject.SetActive(true);
        // RenderSettings.skybox.SetFloat("_Exposure", Mathf.Sin(Time.time * Mathf.Deg2Rad * 100) + 1);
        // RenderSettings.skybox.SetFloat("_Exposure", 0.01f);
        // RenderSettings.skybox.SetFloat("_SunSize", 0f);
    }
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Object exited the trigger and is moving upwards!");
    }
}
