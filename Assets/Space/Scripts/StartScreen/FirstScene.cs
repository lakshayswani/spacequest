﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstScene : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject spaceShip;
    //private Map3D1 map;
    private float time = 0f;
    GameObject AsteroidSphere;
    private QuadController qc;
    Vector3 initialPosition;
    FMOD.Studio.EventInstance EnemyEscapedNarration;
    public GameObject targetPlanet;

    public bool reachedPlanet;

    ScoreBoard sb;

    void Start()
    {
        EnemyEscapedNarration = FMODUnity.RuntimeManager.CreateInstance("event:/SpaceShip/EnemySpaceshipEscaped");
        targetPlanet = GameObject.Find("Rayeon");
        spaceShip = GameObject.Find("spaceShip");
        sb = spaceShip.GetComponent<ScoreBoard>();
        //map = spaceShip.GetComponent<Map3D1>();
        //map.enabled = true;
        AsteroidSphere = GameObject.Find("AsteroidSphere");
        qc = spaceShip.GetComponent<QuadController>();
        initialPosition = spaceShip.transform.position;

        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Velocity")
            {
                child.gameObject.SetActive(true);
                break;
            }
        }

        Rigidbody rb = spaceShip.GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = false;
        //spaceShip.GetComponent<ThrottleReacts>().enabled = true;
        //spaceShip.GetComponent<Ship>().canControl = true;
        //qc.enableControl = false;
    }

    // Update is called once per frame
    private bool asteroidAlert = false;
    private bool autoPilot = false;
    void Update()
    {
        if (!autoPilot)
        {
            //EnemyEscapedNarration.start();
            StartCoroutine(qc.AutoPilotMessage());
            autoPilot = true;
        }
        if (qc.enableControl)
        {
            spaceShip.GetComponent<ThrottleReacts>().enabled = true;
            spaceShip.GetComponent<Ship>().canControl = true;
            qc.enableControl = false;
        }
        time += Time.deltaTime;
        if (!asteroidAlert && Vector3.Distance(spaceShip.transform.position, initialPosition) > 1000)
        {
            StartCoroutine(qc.AsteroidAlert());
            asteroidAlert = true;
            autoPilot = true;
        }
        if (qc.enableAsteroids)
        {
            Debug.Log("called on line 75 - firstscene");
            AsteroidSphere.GetComponentInChildren<AsteroidField>().enabled = true;
            string difficulty = PlayerPrefs.GetString("Difficulty");
            AsteroidField af = AsteroidSphere.GetComponentInChildren<AsteroidField>();
            if (difficulty == "Easy")
            {
                af.asteroidQnt = 450;
                af.speed = 0;
            }
            else if (difficulty == "Medium")
            {
                af.asteroidQnt = 750;
                af.speed = 10;
            }
            else if(difficulty == "Hard")
            {
                af.asteroidQnt = 900;
                af.speed = 30;
            }


            qc.enableAsteroids = false;
        }
        if (reachedPlanet)
        {
            sb.endGame();
            sb.getScore();
            spaceShip.GetComponent<Ship>().canControl = false;
            reachedPlanet = false;
        }
        if (qc.displayedScore)
        {
            Debug.Log("Floating scene called");
            //GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("scene_enemy_path");
            GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("floating");
        }
    }
}
