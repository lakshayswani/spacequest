﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Positron;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject pauseScreenOptions;
    bool pauseScreenState = false;
    public GameObject player;
    void Start()
    {
        pauseScreenOptions = GameObject.Find("PauseScreenOptions");
        pauseScreenOptions.active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Pause"))
        {
            Debug.Log("Pause Called");
            ShowHidePauseScreen();
        }
    }

    private void ShowHidePauseScreen()
    {
        if (player == null)
        {
            player = GameObject.Find("CameraRig");
            if (player == null)
                return;
        }
        if(!pauseScreenState)
        {
            pauseScreenOptions.active = true;
            this.transform.position = player.transform.position + player.transform.forward.normalized*10;
            this.transform.rotation = player.transform.rotation;
            ChangeVoyagerState("Pause");
            EventSystem.current.SetSelectedGameObject(GameObject.Find("Resume"));
        }
        else
        {
            pauseScreenOptions.active = false;
            ChangeVoyagerState("Play");
        }
        pauseScreenState = !pauseScreenState;
    }
    public void Resume()
    {
        ShowHidePauseScreen();
    }

    public void MainMenu()
    {
        ShowHidePauseScreen();
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_1");
    }

    public void Quit()
    {
        ShowHidePauseScreen();
        VoyagerDevice.Stop();
    }

    private void ChangeVoyagerState(string state)
    {
        if(state == "Play")
            VoyagerDevice.Play();
        else
            VoyagerDevice.Pause();
    }


    public void Restart()
    {
        ShowHidePauseScreen();
        GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel(SceneManager.GetActiveScene().name);       
    }
}
