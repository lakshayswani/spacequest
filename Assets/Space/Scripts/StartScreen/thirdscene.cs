﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thirdscene : MonoBehaviour
{
    private AudioSource[] universalSoundObject =null;
    GameObject quad;
    GameObject wm1;
    GameObject wm2;
    GameObject choiceText;
    GameObject riderChoiceMessage;
    AudioSource[] soundSource;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("Scene_4_spaceship"))
        {
            GameObject spaceShip = GameObject.Find("spaceShip");
            Vector3 temp = spaceShip.transform.localEulerAngles;
            temp.y = PlayerPrefs.GetFloat("Scene_4_spaceship");
            spaceShip.transform.localEulerAngles = temp;
            PlayerPrefs.DeleteKey("Scene_4_spaceship");
        }
        riderChoiceMessage = GameObject.Find("RiderChoiceMessage");
        riderChoiceMessage.active = false;
        GameObject uso = GameObject.Find("UniversalSoundObject");

        GameObject ss = GameObject.Find("Scene4SoundObject");
        if (ss != null)
            soundSource = ss.GetComponents<AudioSource>();

        if (uso!=null)
            universalSoundObject = uso.GetComponents<AudioSource>();

        quad = GameObject.Find("Quad");
        foreach (Transform child in GameObject.Find("WelcomeMessage").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "WelcomeMessage1")
            {
                wm1 = child.gameObject;
            }
            if (child.gameObject.name == "WelcomeMessage2")
            {
                wm2 = child.gameObject;
            }
        }

    }

    // Update is called once per frame
    bool initialMessage = true;
    void Update()
    {
        if (initialMessage)
        {
            StartCoroutine(SpaceshipMessage());
            initialMessage = false;
        }
    }


    private IEnumerator SpaceshipMessage()
    {


        Animator an = quad.GetComponent<Animator>();
        an.SetTrigger("buttonin" +
            "coming");
        yield return new WaitForSeconds(1f);
        an.SetTrigger("choice");
        yield return new WaitForSeconds(1f);
        wm1.SetActive(true);
        soundSource[0].Play();
        yield return new WaitForSeconds(10.5f);
        wm1.SetActive(false);
        wm2.SetActive(true);
        soundSource[1].Play();
        yield return new WaitForSeconds(11.5f);
        wm2.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        riderChoiceMessage.active = true;
        soundSource[2].Play();
        GameObject.Find("ThirdScene").GetComponent<rideChoice>().enabled = true;
    }
}
