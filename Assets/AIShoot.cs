﻿using System.Collections;
using UnityEngine;

public class AIShoot : MonoBehaviour
{
    //private UnityEngine.AI.NavMeshAgent agent;
    public State state;
    public Transform gunTip;
    public GameObject fireball;
    private GameObject Player;//Player
    private AIEnemyScript myScenes;
    private GameObject health;
    //private GameObject cylinder;

    private Vector3 target;//coordinates of the target
    private Vector3 moveDirection;//movement direction
    private Vector3 lookDirection;//look direction
    //public float health;//health count
    private GameObject healthPlayer;
    //private GameObject cylinderPlayer;
    private int attackCounts;
    public float fireRate = 1.5f;
    public float nextFire = 0;

    public Vector3 lastKnownPos;
    private bool attacking;
    //private CylinderHealthBar chb;
    //public bool canRest;
    //public bool aggressive;
    //public bool alive = true;//i am alive
    public bool idling = false;//i am idling
    public bool ShowGameOver = false;
    //public LineRenderer laserLineRenderer;

    public enum State
    {//enumeration of states
        Idle,
        Attack,
        CheckLastPos,
    }

    private void Awake()
    {
        myScenes = this.GetComponent<AIEnemyScript>();
        //agent = (UnityEngine.AI.NavMeshAgent)this.GetComponent("NavMeshAgent");
        Player = (GameObject)GameObject.FindGameObjectWithTag("Player");
        attackCounts = 0;
        state = AIShoot.State.Idle;
        StartCoroutine("FSMach");//start our state machine	
        //Idle();

        //Debug.Log("inside awake");
    }

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("inside start of aishoot");
        //chb = GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>();
        //GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>().enabled = true;

        GameObject.Find("spaceShip").GetComponent<PlayerHealth>().enabled = true;
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "HealthBar")
            {
                health = child.gameObject;
                break;
            }
        }
        health.SetActive(true);

        /*
        GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>().enabled = true;
        float y_inc = 0.0000283f;
        while (health.transform.localScale.y < 0.085)
        {
            health.transform.localScale += new Vector3(0, y_inc, 0);
        }
        foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
        {
            if (child.gameObject.name == "Cylinder")
            {
                cylinder = child.gameObject;
                break;
            }
        }
        cylinder.SetActive(true);
        GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>().enabled = true;
        while (cylinder.transform.localScale.y < 0.085)
        {
            cylinder.transform.localScale += new Vector3(0, y_inc, 0);
        }
        */
    }

    private float endTime = 0f;
    // Update is called once per frame
    void Update()
    {
        if (ShowGameOver)
        {
            endTime += Time.deltaTime;
            if (endTime > 5.0f)
            {
            //s    GameObject.Find("LevelChanger").GetComponent<LevelChanger>().FadeToLevel("Scene_1");
            }
        }
    }

    private IEnumerator FSMach()
    {
        //while (attackCounts < 10000 && CylinderHealthBar.lives > 0)
            while (attackCounts < 10000 && PlayerHealth.lives > 0)
            {//if character is still alive
        //    Debug.Log("inside coroutine");
            switch (state)
            {
                case State.Attack:
                    Attack();
                    break;
                case State.Idle:
                    Idle();
                    break;
                case State.CheckLastPos:
                    CheckLastPos();
                    break;
            }
            yield return null;
        }
    }

    private void Attack()
    {//attack state
        idling = false;//we're not idling anymore
    //    speed = 2;//change movement speed from 0 to 2
    //    agent.speed = 0;
        attacking = true;
        target = Player.transform.position; // Player is current target
    //    Moving(); //run movement function
        if (((Player.transform.position - this.transform.position).magnitude > 3000f))
        {//player is not visible
            //Debug.Log("Player not visible");
            lastKnownPos = Player.transform.position;
            state = AIShoot.State.CheckLastPos;//scheck last pos
        }
        
        if (((Player.transform.position - this.transform.position).magnitude < 3000f))
        {//if distance to player is lesser than 2 or character is hitted
         //    speed = 0;//stop movement
         //    agent.speed = 0;
            //Debug.Log("Player visible within 100f");
            lookDirection = target - this.transform.position;  // calculating look  direction (towards target)
        //    lookDirection.y = 0; // restricting Y axis rotation
            Quaternion newRot = Quaternion.LookRotation(lookDirection);//rotation that looks along forward with the the head upwards along upwards
            //this.transform.rotation = Quaternion.Slerp(transform.rotation, newRot, Time.deltaTime * 15); //smooth linear interpolation from our current rotation to new rotation
            /*if(attackCounts < 10000 && CylinderHealthBar.lives > 0)
            {
                //ShootRay();
                ShootFireball();
            }*/
            if (attackCounts < 10000 && PlayerHealth.lives > 0)
            {
                //ShootRay();
                ShootFireball();
            }
            else
            {
                //Debug.Log("Already hit twice from the enemy spaceship or no health");
            }

        }
        //else
        //{//if distance to player is bigger than 2 and character is not hitted
        //    speed = 2;//move character faster
        //    agent.speed = 1f;
        //}
        else if (((Player.transform.position - this.transform.position).magnitude > 3000f))
        {//if player escaped from character (distance between tham greater than 20)
            //Debug.Log("Player away more than 100f");
            lastKnownPos = Player.transform.position;
            state = AIShoot.State.CheckLastPos;//scheck last pos
        }
    }

    private void Idle()
    {//idling state
     //    speed = 0;
     //    agent.speed = 0;
    //    Debug.Log("inside Idle()");
        idling = true;
        attacking = false;
        if (idling)
        {
        //    if (idlingTime > 0)//while idling time is greater than 0
        //        idlingTime -= Time.deltaTime;//descrease idling time by Time.deltaTime
            if (((Player.transform.position - this.transform.position).magnitude < 3000f) && ShowGameOver == false)
            {
                idling = false;//we're not idling anymore
                state = AIShoot.State.Attack;
            }
        }
    }

    //CHECK PLAYER'S LAST KNOWN POSITION
    private void CheckLastPos()
    {
        idling = false;
        target = lastKnownPos;
        //Moving();
        //speed = 1;//if we're patroling lets move slowly
        //agent.speed = 0.1f;
        float distToPos = Vector3.Distance(this.transform.position, target);
        if (distToPos < 1f)
        {
            idling = true;
            state = AIShoot.State.Idle;
        }
        if (((Player.transform.position - this.transform.position).magnitude < 3000f) && ShowGameOver == false)
        {
            idling = false;//we're not idling anymore
            state = AIShoot.State.Attack;
        }
    }

    public void ShootFireball()
    {
        Debug.Log("Shoot Fireball");    
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            GameObject bolt = Instantiate(fireball, gunTip.position, gunTip.rotation);
            Destroy(bolt, 4f);
            attackCounts++;
        }
    }

    public void ShootRay()
    { //called by animation event
        //Debug.Log("Entered shooting ray");
        Vector3 direction = ((Player.transform.position + transform.up * Random.Range(0f, 3.5f) + transform.right * Random.Range(-.7f, .7f)) - gunTip.position).normalized;//added 2.5 offset because players pivot is at 0
        Ray ray = new Ray(gunTip.position, direction);
        RaycastHit hit;
        attackCounts++;
        //Debug.DrawRay(gunTip.position, direction * 10, Color.red, 100);
        //laserLineRenderer.enabled = true;
        //laserLineRenderer.SetPosition(0, gunTip.position);
        //laserLineRenderer.SetPosition(1, direction * 10);
        //laserLineRenderer.enabled = true;
        //gunASource.PlayOneShot(gunShot);    // audio
        if (Physics.Raycast(ray, out hit, 100f))
        {
            //Debug.Log("physics raycast");
            //Debug.DrawLine(ray.origin, hit.point, Color.red);
            //laserLineRenderer.enabled = true;
            //laserLineRenderer.SetPosition(0, ray.origin);
            //laserLineRenderer.SetPosition(1, hit.point);
            //laserLineRenderer.enabled = true;
            //Debug.Log(hit.collider.gameObject.name);
            if (hit.collider.gameObject.tag == "Player")
            {
                /*
                // display health bar
                foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
                {
                    if (child.gameObject.name == "HealthBar")
                    {
                        healthPlayer = child.gameObject;
                        break;
                    }
                }
                healthPlayer.SetActive(true);
                GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>().enabled = true;
                float y_inc = 0.0000283f;
                while (healthPlayer.transform.localScale.y < 0.085)
                {
                    healthPlayer.transform.localScale += new Vector3(0, y_inc, 0);
                }
                foreach (Transform child in GameObject.Find("cockpit").GetComponentInChildren<Transform>())
                {
                    if (child.gameObject.name == "Cylinder")
                    {
                        cylinderPlayer = child.gameObject;
                        break;
                    }
                }
                cylinderPlayer.SetActive(true);
                GameObject.Find("spaceShip").GetComponent<CylinderHealthBar>().enabled = true;
                while (cylinderPlayer.transform.localScale.y < 0.085)
                {
                    cylinderPlayer.transform.localScale += new Vector3(0, y_inc, 0);
                }
                */

                /*
                CylinderHealthBar.lives--;
                //Debug.Log("health: " + CylinderHealthBar.lives);
                if (CylinderHealthBar.lives <= 0)
                {
                    ShowGameOver = true;
                }
                */

                PlayerHealth.lives--;
                if (PlayerHealth.lives <= 0)
                {
                    ShowGameOver = true;
                }

            }
            else
            {
                //Debug.DrawLine(ray.origin, ray.origin + ray.direction * 100, Color.yellow);
            }
        }
    }
}
