﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using UnityEngine;

public class enemyShip : MonoBehaviour
{
    // Start is called before the first frame update
    public float startTakeOffTime;
    public float endTakeOffTime = 1f;
    private float length = 2f;
    public float endForwardTime;

    public float endTime;
    public float forwardDoneTime = 16f;

    public bool isMainSpaceShip;

    public bool isRight;

    private int turnRight;
    private GameObject spaceShip;
    private Rigidbody rigidBody;
    private Animator _anim;
    public bool landed;
    private float time;
    private Animator _animator;
    private bool takenOff;
    private ParticleSystem starDust;
    private GameObject OuterEarth;
    private float speed = 1000f;
    public float startTime = 4.0f;
    public float endTime1 = 4.0f;
    public float hyperSpeedEndTime = 2.0f;
    private QuadController qc;
    private bool gotQuad = false;

    // Start is called before the first frame update
    void Start()
    {
        spaceShip = GameObject.Find("spaceShip");
        takenOff = false;
        landed = true;
        rigidBody = GetComponent<Rigidbody>();
        turnRight = isRight ? 1 : -1;
        starDust = GameObject.Find("Speed Dust").GetComponent<ParticleSystem>();
        starDust.Stop();
        OuterEarth = GameObject.Find("OuterEarth");
        qc = spaceShip.GetComponent<QuadController>();

    }

    // Update is called once per frame
    void Update()
    {
        Play();
        time += Time.deltaTime;
        if (!takenOff && time > (startTakeOffTime) && time < endTakeOffTime)
        {
            MoveSpaceShipUp(50000f);
        }
        if (time > endTakeOffTime && time < endTakeOffTime + 1f)
        {
            MoveSpaceShipUp(50000f);
        }
    }

    void MoveSpaceShipUp(float speed)
    {
        rigidBody.AddForce(transform.forward * Time.deltaTime * speed * rigidBody.mass);
    }

    void Play()
    {
        starDust.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.Stretch;
        if (time > startTime && time < endTime1)
        {
            length = Mathf.MoveTowards(length, 10, Time.deltaTime);
            starDust.GetComponent<ParticleSystemRenderer>().lengthScale = length;
            starDust.Play();
        }
        else if (time > hyperSpeedEndTime)
        {
            length = Mathf.MoveTowards(length, 0, Time.deltaTime * 200);
            starDust.GetComponent<ParticleSystemRenderer>().lengthScale = length;
            starDust.Play();
        }
        else if (time > endTime1 && time < hyperSpeedEndTime)
        {
            length = Mathf.MoveTowards(length, 2000, Time.deltaTime * 300);
            starDust.GetComponent<ParticleSystemRenderer>().lengthScale = length;
            starDust.Play();
        }


        if (OuterEarth != null)
        {
            if (time < startTime)
            {

            }
            else if (time < endTime1)
            {
                speed = Mathf.MoveTowards(speed, 0.1f, Time.deltaTime * 0.1f);
                OuterEarth.transform.Translate(OuterEarth.transform.forward * Time.deltaTime * 1 * speed * 0.4f);
            }
            else
            {
                speed = Mathf.MoveTowards(speed, 1f, Time.deltaTime * 1f);
                OuterEarth.transform.Translate(OuterEarth.transform.forward * Time.deltaTime * 1 * speed * 2f);
            }

            if (!gotQuad && time > endTime + 3f)
            {
                StartCoroutine(qc.BroadCastInitialMessage());
                gotQuad = true;
            }

            if (time > hyperSpeedEndTime && length <= 10)
            {
                GameObject.Find("LevelChanger1").GetComponent<LevelChanger>().FadeToLevel("Scene_2");
            }
        }

    }

    IEnumerator MoveShip(GameObject go, bool left)
    {
        if (go.name == "ship2")
        {
            go.GetComponent<Animator>().enabled = false;
        }
        rigidBody = go.GetComponent<Rigidbody>();
        int invert = left ? -1 : 1;
        rigidBody.AddForce(transform.forward * Time.deltaTime * 7000 * rigidBody.mass);
        rigidBody.AddTorque(transform.up * Time.deltaTime * 7 * invert);
        yield return null;
    }
}
